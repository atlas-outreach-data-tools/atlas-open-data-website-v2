/**
 * Creating a sidebar enables you to:
 - create an ordered group of docs
 - render a sidebar for each doc of that group
 - provide next/previous navigation

 The sidebars can be generated from the filesystem, or explicitly defined here.

 Create as many sidebars as you want.
 */

// @ts-check

/** @type {import('@docusaurus/plugin-content-docs').SidebarsConfig} */
const sidebars = {
  // intro: [
  //   'about',
  //   {
  //     type: 'category',
  //     label: 'Introduction',
  //     collapsed: false,
  //     items: [
  //       'atlas/lhc', 
  //       'atlas/experiment', 
  //       'atlas/atlas_events'
  //     ],
  //   }
  //   ],

  paths:[
    {
      type: 'category',
      label: 'Get Started',
      link: {
        type: 'generated-index',
        title: 'Get Started',
        description: 'In this section you will find different suggested paths to get involved with ATLAS Open Data. This are just suggestions on what we think it will be more usefull to check in each case. However, feel free to check the website freely.',
      },
      items: [
        'userpath/quickstart',
        'userpath/deepdive',
        'userpath/researchers',
        'visualization/index'
      ]
    }
  ],

  data: [
    {
      type: 'category',
      label: 'The Data',
      link: {
        type: 'doc',
        id: 'data/index',
      },
      items: [
        {
          type: 'category',
          label: 'Data for education',
          link: {
            type: 'generated-index',
            title: 'Data for education',
            description: 'For educational data, we currently offer two different datasets: one at a center-of-mass energy of 8 TeV, corresponding to data taken in 2012, and another at 13 TeV, corresponding to data taken in 2016.'
          },
          items: [
            {
              type: 'category',
              label: '8 TeV Data',
              link: {
                type: 'doc',
                id: 'data/for_education/8TeV_details'
              },
              items: [
                'data/for_education/8TeV_metadata',
                'data/for_education/8TeV_whattodo'
              ],
            },
            {
              type: 'category',
              label: '13 TeV Data',
              link: {
                type: 'doc',
                id: 'data/for_education/13TeV_details'
              },
              items: [
                'data/for_education/13TeV_access',
                'data/for_education/13TeV_whattodo'
              ],
            }
          ],
        },
        {
          type: 'category',
          label: 'Data for research',
          link: {
            type: 'generated-index',
            title: 'Data for research',
            description: 'For research data, we offer two different collections: proton-proton collisions and heavy ion collisions. Both from the run period of 2015 and 2016.'
          },
          items: [
            {
              type: 'category',
              label: 'Proton-proton Collisions',
              link: {
                type: 'doc',
                id: 'data/for_research/pp_data'
              },
              items: [
                'data/for_research/metadata',
              ],
            },
            {
              type: 'category',
              label: 'Heavy Ion Collisions',
              link: {
                type: 'doc',
                id: 'data/for_research/hi_data'
              },
              items: [
              ],
            },
            'data/for_research/data_access',
            
          ],
        },
      ]
    }
  ],

  tutresearch: [
    {
      type: 'category',
      label: 'Tutorials for Research',
      link: {
        type: 'generated-index',
        title: 'Tutorials for Open Data for Research',
        description: 'Learn about the most important concepts!',
      },
      items: [
        'tutresearch/physlitetut',
        'tutresearch/containers',
        'tutresearch/phoenix',
        'tutresearch/public_likelihoods',
        'tutresearch/contributions'
      ],
    }
  ],

  tutedu: [
    {
      type: 'category',
      label: '8 TeV Tutorials for Education',
      link: {
        type: 'generated-index',
        title: '8 TeV Open Data Tutorials for Education',
        description: 'Learn about the most important concepts!',
      },
      items: [
      '8TeVDoc/gettingstarted',
      '8TeVDoc/take_a_look_at_the_data',
      '8TeVDoc/take_a_closer_look',
      '8TeVDoc/histograms',
      '8TeVDoc/more_histograms',
      '8TeVDoc/create-a-new-plot',
      '8TeVDoc/event_selection',
      '8TeVDoc/navigator',
      ],
    },
    {
      type: 'category',
      label: '13 TeV Tutorials for Education',
      link: {
        type: 'generated-index',
        title: '13 TeV Open Data Tutorials for Education',
        description: 'Welcome to the 13 TeV tutorial. Please, start by choosing your enviroment and continue as needed.',
      },
      items: [
        {
          type: 'category',
          label: 'Choose your enviroment',
          link: {
            type: 'generated-index',
            title: 'Choose your enviroment',
            description: 'We have multiple enviroment options so you can engage with the ATLAS open data as best fits you. Select the option that is more convenient for your situation or use case.'
          },
          items: [
            '13TeVDoc/enviroments/online',
            '13TeVDoc/enviroments/hybrid',
            'saas/index'
          ],
        },
        {
          type: 'category',
          label: 'Analysis frameworks',
          link: {
            type: 'generated-index',
            title: 'Analysis frameworks',
            description: 'The 13 TeV ATLAS Open Data, available through the CERN Open Data portal, is supported by a suite of analysis frameworks. These tools, designed for educational use at various levels, are developed in C++ and compatible with ROOT and Python. They are ideal for hands-on physics exercises, providing capabilities for data analysis, histogram generation, and result plotting. Basic C++ knowledge is sufficient to utilize these frameworks, which are publicly accessible on GitHub.',
          },
          items: [
            '13TeVDoc/frameworks/cpp',
            '13TeVDoc/frameworks/uproot',
            '13TeVDoc/frameworks/pyroot',
            // 'notebooks/framework-interface',
            // 'notebooks/intro'
          ],
        },
        {
          type: 'category',
          label: 'Analysis notebooks',
          link: {
            type: 'generated-index',
            title: 'Analysis notebooks',
            description: 'As part of our 13 TeV release for education, we offer notebooks with different types of analysis to get you started using the open data. Choose the type of analysis that you would like to check.',
          },
          items: [
            '13TeVDoc/13tutorial',
            '13TeVDoc/13tutorial-BSM',
            '13TeVDoc/13tutorial-train',
            'status'
            // 'notebooks/framework-interface',
            // 'notebooks/intro'
          ],
        },
      ]
    },  
  ],

  videotuts: [
    {
      type: 'category',
      label: 'Video Tutorials',
      items: [
        'videotutorials/overview',
      ],
    }
  ],
    
  visualization: [
    {
      type: 'category',
      label: 'Online Data Analyser',
      link: {
        type: 'doc',
        id: 'visualization/index'
      },
      items: [
        'visualization/curriculum',
        'visualization/the-higgs-boson',
        'visualization/atlas_events',
        'visualization/analyses',
        'visualization/data-and-simulated-data_13TeV',
        'visualization/the_display_histograms_13TeV',
        'visualization/histogram-analyser-2_13TeV',
        'visualization/separate_signals_13TeV',
        'visualization/find_the_higgs_2_13TeV',
        'visualization/rare-top-processes',
        'visualization/ttZanalyses',
        'visualization/histogram-analyser-ttZ',
        'visualization/separate_signals_ttZ',
        'visualization/find_ttZ'
      ],
    }
  ],
  
  documentation: [
    {
      type: 'category',
      label: 'ATLAS Open Data',
      link: {
        type: 'doc',
        id: 'documentation/introduction/introductory_page'
      },
      items: [
      ],
    },
    {
      type: 'category',
      label: 'ATLAS and the Large Hadron Collider',
      link: {
        type: 'doc',
        id: 'documentation/introduction/introduction_LCH'
      },
      items: ['documentation/introduction/introduction_ATLAS'],
    },
    {
      type: 'category',
      label: 'The Standard Model and Beyond',
      link: {
        type: 'doc',
        id: 'documentation/introduction/SM_and_beyond'
      },
      items: [],
    },
    {
      type: 'category',
      label: 'Introduction to Heavy-Ion Physics',
      link: {
        type: 'doc',
        id: 'documentation/introduction/heavy_ions'
      },
      items: [],
    },
    {
      type: 'category',
      label: 'CERN Open Data Policy',
      link: {
          type: 'doc',
          id: 'documentation/introduction/purpose_data_release',
          },
      items: [
          'documentation/introduction/purpose_data_education',
          'documentation/introduction/purpose_data_research',      
      ],
    },
    {
      type: 'category',
      label: 'Data Formats',
      link: {
        type: 'doc',
        id: 'documentation/data_format/introduction_data_format',
      },
      items: [
        'documentation/data_format/ntuple',
        'documentation/data_format/physlite',
        'documentation/data_format/HI_formats'
      ],
    },
    {
      type: 'category',
      label: 'Data Collection',
      link: {
        type: 'doc',
        id: 'documentation/data_collection/data_collection',
      },
      items: [
        'documentation/data_collection/GRL_definition'
      ],
    },
    {
      type: 'category',
      label: 'Simulated Data',
      link: {
        type: 'doc',
        id: 'documentation/monte_carlo/introduction_MC',
      },
      items: [
        'documentation/monte_carlo/MC_production_chain',
        'documentation/monte_carlo/simulation_tools',
      ],
    },
    {
      type: 'category',
      label: 'Physics Objects',
      link: {
        type: 'doc',
        id: 'documentation/physic_objects/physic_objects_intro'
      },
      items: [
        {
        type: 'category',
        label: 'Electrons and Photons',
        link: {
          type: 'doc',
          id: 'documentation/physic_objects/electronphoton',
          },
        items: ['documentation/physic_objects/electronphoton_reco',
                'documentation/physic_objects/electronphoton_calibration',
                //'documentation/physic_objects/electronphoton_identification',
          ]
        },
        {
          type: 'category',
          label: 'Jets',
          link: {
            type: 'doc',
            id: 'documentation/physic_objects/jets',
            },
          items: ['documentation/physic_objects/jets_reco',
                  'documentation/physic_objects/jets_calibration',  
            ]
          },
      ],
    },
    {
      type: 'category',
      label: 'Setting Uncertainties',
      link: {
        type: 'doc',
        id: 'documentation/systematics/systematics',
      },
      items: [
      ],
    },
    // {
    //   type: 'category',
    //   label: 'Data Analysis Guidelines',
    //   link: {
    //     type: 'doc',
    //     id: 'documentation/data_analysis_guidelines/data_analysis_intro',
    //   },
    //   items: [
    //   ],
    // },
    {
      type: 'category',
      label: 'Example Analysis',
      link: {
        type: 'doc',
        id: 'documentation/example_analyses/example_analyses_intro',
      },
      items: [
        'documentation/example_analyses/analysis_examples_education_2020'
      ],
    },
    {
      type: 'category',
      label: 'Citing ATLAS',
      link: {
        type: 'doc',
        id: 'documentation/ethical_legal/citation_policy',
      },
      items: [
      ],
    },
    {
      type: 'category',
      label: 'Support and Resources',
      items: [
        'documentation/support_resources/technical_support',
        'documentation/support_resources/resources'
      ],
    },
    {
      type: 'category',
      label: 'Glossary',
      link: {
        type: 'doc',
        id: 'documentation/appendix/glossary',
      },
      items: [
      ],
    },
  ]
};

module.exports = sidebars;
