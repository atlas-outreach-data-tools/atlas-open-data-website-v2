# Video Tutorials for Open Data

We have multiple video tutorials available on YouTube that you can use as a starting point to learn more about how to set-up the software required to do analysis with ATLAS Open Data and how to ananalyse the data.

For example you can learn how to rediscover the Higgs boson! But you can also learn for example on how to create histograms with PyROOT!

The available video tutorials are:

#### [Find the Higgs boson with your mouse](https://www.youtube.com/watch?v=X1PyNTUwffI&list=PL1qU3k-RDRsvy3jhxUTmq7ZJQTdFvjLJn&index=1)
<a href="https://www.youtube.com/watch?v=X1PyNTUwffI&list=PL1qU3k-RDRsvy3jhxUTmq7ZJQTdFvjLJn&index=1" target="_blank">
 <img src="http://img.youtube.com/vi/X1PyNTUwffI/hqdefault.jpg" alt="Watch the video" width="360" height="270" border="10" />
</a>

This ATLAS Open Data tutorial will teach you how to find the Higgs boson using just your mouse!
What you'll learn in this video:
- how to interact with particle physics histograms
- the principle of selection criteria
- the idea of signal and background
- the different decays of the Higgs boson and how to find it

#### [Data analysis in a web browser](https://www.youtube.com/watch?v=wIr_WZHpMSc&list=PL1qU3k-RDRsvy3jhxUTmq7ZJQTdFvjLJn&index=2)
<a href="https://www.youtube.com/watch?v=wIr_WZHpMSc&list=PL1qU3k-RDRsvy3jhxUTmq7ZJQTdFvjLJn&index=2" target="_blank">
 <img src="http://img.youtube.com/vi/wIr_WZHpMSc/hqdefault.jpg" alt="Watch the video" width="360" height="270" border="10" />
</a>

Follow along this ATLAS Open Data tutorial and learn how to carry out data analysis directly from your web browser using a Jupyter Notebook! Jupyter notebooks allow you to easily interact with ATLAS Open Data.
What you'll learn in this video:
- how to interact with ATLAS Open Data without downloading files or writing code
- finding, opening and running a Jupyter Notebook from your web browser

#### [How to rediscover the Higgs boson](https://www.youtube.com/watch?v=aOz-Xj_znrs&list=PL1qU3k-RDRsvy3jhxUTmq7ZJQTdFvjLJn&index=3)
<a href="https://www.youtube.com/watch?v=aOz-Xj_znrs&list=PL1qU3k-RDRsvy3jhxUTmq7ZJQTdFvjLJn&index=3" target="_blank">
 <img src="http://img.youtube.com/vi/aOz-Xj_znrs/hqdefault.jpg" alt="Watch the video" width="360" height="270" border="10" />
</a>

This ATLAS Open Data tutorial will teach you how to rediscover and analyse the Higgs boson.
What you'll learn in this video:
- how to carry out particle physics analyses
- how code is used to analyse particle physics collisions
- how to rediscover the Higgs boson using a Jupyter notebook

#### [Installing a VirtualBox](https://www.youtube.com/watch?v=oqaXNWW6q5w&list=PL1qU3k-RDRsvy3jhxUTmq7ZJQTdFvjLJn&index=4)
<a href="https://www.youtube.com/watch?v=oqaXNWW6q5w&list=PL1qU3k-RDRsvy3jhxUTmq7ZJQTdFvjLJn&index=4" target="_blank">
 <img src="http://img.youtube.com/vi/oqaXNWW6q5w/hqdefault.jpg" alt="Watch the video" width="360" height="270" border="10" />
</a>

Follow this ATLAS Open Data tutorial and learn how to install the VirtualBox software directly from your web browser! VirtualBox allows you to manipulate more than one Virtual Machine on your computer. We will use it to get our own data analysis environment.
What you'll learn in this video:
- how to install VirtualBox

#### [Installing a Virtual Machine](https://www.youtube.com/watch?v=vwa8rRYXqOs&list=PL1qU3k-RDRsvy3jhxUTmq7ZJQTdFvjLJn&index=5)
<a href="https://www.youtube.com/watch?v=vwa8rRYXqOs&list=PL1qU3k-RDRsvy3jhxUTmq7ZJQTdFvjLJn&index=5" target="_blank">
 <img src="http://img.youtube.com/vi/vwa8rRYXqOs/hqdefault.jpg" alt="Watch the video" width="360" height="270" border="10" />
</a> 

Do you want your own “analysis server”? This ATLAS Open Data tutorial will teach you how to install a Virtual Machine!
Virtual Machines allow you to run an operating system in an app window on your desktop that behaves like a separate computer, or in this case, as a server. After that, you’ll only need an internet browser.
What you'll learn in this video:
- how to install Virtual Machine
- how to use VirtualBox to open and run a Virtual Machine

#### [Making Selection Cuts with PyROOT](https://www.youtube.com/watch?v=FhboBZuLC08&list=PL1qU3k-RDRsvy3jhxUTmq7ZJQTdFvjLJn&index=6)
<a href="https://www.youtube.com/watch?v=FhboBZuLC08&list=PL1qU3k-RDRsvy3jhxUTmq7ZJQTdFvjLJn&index=6" target="_blank">
 <img src="http://img.youtube.com/vi/FhboBZuLC08/hqdefault.jpg" alt="Watch the video" width="360" height="270" border="10" />
</a>

Accompanied by a Jupyter Notebook, this video will lead you further into PyROOT data analysis to teach you how to code selection cuts and explain some of the key physical phenomena happening when particles collide at the LHC.
What you'll learn in this video:
- Understanding the basics behind High Energy Particle Physics at the LHC, four-vector, and four-momentum
- Coding selection cuts and defining four-momentum in PyROOT

#### [Create Histograms with PyROOT](https://www.youtube.com/watch?v=cYNUyfn-ido&list=PL1qU3k-RDRsvy3jhxUTmq7ZJQTdFvjLJn&index=7)
<a href="https://www.youtube.com/watch?v=cYNUyfn-ido&list=PL1qU3k-RDRsvy3jhxUTmq7ZJQTdFvjLJn&index=7" target="_blank">
 <img src="http://img.youtube.com/vi/cYNUyfn-ido/hqdefault.jpg" alt="Watch the video" width="360" height="270" border="10" />
</a>

Following a Jupyter Notebook, this video will teach you the fundamentals of PyROOT concepts and syntax as well as briefly explaining the Physics behind the example data analysis.
What you'll learn in this video:
- Understanding the basics behind the Standard Model and Feynman diagrams
- Manipulating the partitions of a ROOT file
- Plotting charts and populating histograms with PyROOT
