---
title: "Build your own infrastructure"
tags: ["Terraform", "Cloud", "Notebooks", "Jupyter", "Software", "SaaS"]
summary: "Recipes for infrastructure deployment"
---

# Software as a Service

Welcome to the Automated Infrastructure Deployment Tool!
Did you ever wonder how to obtain a working, multi-user analysis environment for your students or your group?<br/>
This page will guide you step by step in the setup of a [JupyterHub](https://jupyter.org/hub) hosted on a Cloud instance, and will provide an overview of few notebooks solutions, to implement both **standalone or in combination** with [JupyterHub](https://jupyter.org/hub).

Here you have an advanced tool that allows **anyone** to deploy his/her own [JupyterHub](https://jupyter.org/hub). <br/>
Why anyone? Because no particular skills are required, it is sufficient to have a _basic knowledge of the linux bash terminal_, and access to a _cloud provider_ onto which host the whole infrastructure.

## Features

Now, let's dive into the fundamental features of the tool: <br/>
(if you don't care about the theory, just jump to the [dashboard](#deployment-dashboard) and start deploying!)

* **Notebooks flexibility**: your [JupyterHub](https://jupyter.org/hub) will not be bound to a specific notebook. We do provide a [standard notebook](#single-user-solutions), but if you have your own notebook image available on a registry, you can effortlessly deploy it. This allows both great customizability, and the possibility to exploit [JupyterHub](https://jupyter.org/hub) for different analyses (not only ALTAS Open Data, but also ML applications, etc..)
* **Persistency of data**: your work will not be lost at the end of the session, neither you will have a timeout for your notebooks. Data, plots and results are always saved and accessible.
* **External/shared volumes**: if you have a big dataset, it is crucial to be able to share it with the users without having each of them to download the data.
* **Speed**: this infrastructure is _fast_: the deploy time of the whole environment is of the order of 10 minutes (it only happens once); once [JupyterHub](https://jupyter.org/hub) is up and running, users will be able to obtain their notebook in a matter of _seconds_.

---

<div align="center">

![](./images/workflow.png)

</div>

## Workflow

The deployment process relies on **two** fundamental services: [Docker](https://www.docker.com/) and [Terraform](https://www.terraform.io/). <br/>
Docker provides the **modular** layout of the infrastructure, enabling most of the above-mentioned features; together with agility and flexibility, thanks to the containerization, Docker also provides lightness to the whole environment. On the other hand, Terraform ensures the **automation** of the [JupyterHub](https://jupyter.org/hub) deployment. Below it is shown a flowchart of the deployment process.

---

## Single user solutions

### Container images for Jupyter notebooks
[Jupyter notebooks](https://jupyter.org/) are the core of the analysis effort of ATLAS Open Data. <br/>
Here we present a notebook built on top of the latest [scipy](https://hub.docker.com/r/jupyter/scipy-notebook) version, in which we embed the installation of the [ROOT](https://root.cern/) framework used at CERN, available in both python and C++ kernels. Other versions of the notebook are in development, in order to gradually increase the analysis potential of this tool.<br/>
You can run the notebook both **standalone or in combination** with the [JupyterHub](https://jupyter.org/hub) infrastructure descripted above. Try it out on your pc by simply running the following docker command:
```
docker run -it --rm -p8888:8888 atlasopendata/root_notebook:latest
```
Then access the notebook via the browser by pasting the url provided by the terminal. It should be something like
(The token will be different for you)
```
To access the notebook, open this file in a browser:
        file:///home/jovyan/.local/share/jupyter/runtime/nbserver-15-open.html
    Or copy and paste one of these URLs:
        http://4c61742ed77c:8888/?token=34b7f124f6783e047e796fea8061c3fca708a062a902c2f9
     or http://127.0.0.1:8888/?token=34b7f124f6783e047e796fea8061c3fca708a062a902c2f9
```

### Virtual Machine(s)
ATLAS Open Data also provides a [virtual machine](http://opendata.atlas.cern/release/2020/documentation/vm/index.html), which allows you to test the 13 TeV ATLAS Open Data on your own host machine.

Take a closer look to our resources:

| <h3><b>Container git repositories</b></h3> | <h3><b>Container Registry</b></h3> | <h3><b>Virtual Machine</b></h3> |
|        :---:        |        :---:       |        :---:       |
| [![JR](./images/jrepo.png)](https://gitlab.cern.ch/atlas-open-data-iac-qt-2021/public-containers) |[![JN](./images/jn.png)](https://hub.docker.com/r/atlasopendata/root_notebook) |[![VM](./images/vm.png)](http://opendata.atlas.cern/release/2020/documentation/vm/index.html) |

&nbsp;

---

## Deployment dashboard

Just select the cloud provider that tickles your fancy (i.e. of which you actually have a working account), and follow the instructions!

| <h3><b>AWS instance</b></h3> | <h3><b>OpenStack@CERN instance</b></h3> | <h3><b>Google Cloud instance</b></h3> |
|        :---:        |        :---:       |        :---:       |
| [![](./images/Amazon-Web-Services-AWS-Logo.png ".")](https://gitlab.cern.ch/atlas-open-data-iac-qt-2021/aws_automated_jh_deployment) | [![](./images/OpenStack-Logo-Vertical.png ".")](https://gitlab.cern.ch/atlas-open-data-iac-qt-2021/automated_jh_deployment) | ![](./images/Google-Cloud-Emblem_work_in_progress.png ".") |


**WARNING**: we are experiencing an issue that prevents Openstack instances to correctly install conda packages. We are investigating on the possible causes.


### Local deployment
If you wish to try the infrastructure out **without** having to use a cloud provider, _no problem_! Just follow [these instructions](https://gitlab.cern.ch/atlas-open-data-iac-qt-2021/jupyterhub-local) and deploy [JupyterHub](https://jupyter.org/hub) **on your pc**. You will be able to explore the environment and experience all the features that come with it!

### Further readings
If you are interested in knowing more about the technical details of this deployment tool, please take a look at:
* [The openstack-deployer gitlab repository](https://gitlab.cern.ch/atlas-open-data-iac-qt-2021/automated_jh_deployment)
* [The AWS-deployer gitlab repository](https://gitlab.cern.ch/atlas-open-data-iac-qt-2021/aws_automated_jh_deployment)
* [The CI notebook images gitlab repository](https://gitlab.cern.ch/atlas-open-data-iac-qt-2021/notebooks-images)


---

### Disclaimer
The code and the recipes are licensed under the [**European Union Public Licence**](https://ec.europa.eu/info/european-union-public-licence_en).<br/>

The datasets are provided by the ATLAS Collaboration only for educational purposes and is not suited for scientific publications.


* The [ATLAS Open Data](http://opendata.atlas.cern) are released under the [Creative Commons CC0 waiver](http://creativecommons.org/publicdomain/zero/1.0/).
* Neither ATLAS nor CERN endorses any works produced using these data, which is intended only for educational use.
* All data sets will have a unique [DOI](https://en.wikipedia.org/wiki/Digital_object_identifier) that you are requested to cite in any (non-scientific) applications or publications.
* Despite being processed, the high-level primary datasets remain complex, and selection criteria need to be applied in order to analyse them, requiring some understanding of particle physics and detector functioning.
* The large majority of the data cannot be viewed in simple data tables for spreadsheet-based analyses.

---
