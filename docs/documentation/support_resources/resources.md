# Additional resources: ATLAS Open Data at conferences

Are you looking for additional documentation? Then the following material might fit your curiosity! <br/>
In this page, you can find a list of the recent talks and posters at conferences presented on ATLAS Open Data by our team. <br/>
Please consider this documentation as a summary on what does the ATLAS Open Data project consist of, and which resources are available for education and research, in a nutshell.<br/>

## Talks at conferences
| Title		       | Conference      | Date       | Author            |
|------------------|-----------------|------------|-------------------|
| [The First Release of ATLAS Open Data for Research](https://indico.cern.ch/event/1338689/contributions/6013332/) | CHEP 2024 | 21 October 2024 | Zach Marshall |
| [Open Data at ATLAS: Bringing TeV collisions to the World](https://indico.cern.ch/event/1338689/contributions/6011129/) | CHEP 2024 | 21 October 2024 | Giovanni Guerrieri |
| [The First Release of ATLAS Open Data for Research](https://indico.cern.ch/event/1291157/contributions/5887163/) | ICHEP 2024 | 8 July 2024 | Mariana Isabel Vivas Albornoz |
| [ATLAS Open Data: developing education and outreach resources from research data](https://indico.jlab.org/event/459/contributions/11675/) | CHEP 2023 | 9 May 2023 | Attila Krasznahorkay |
| [ATLAS Open Data - a genuinely collaborative approach for the creation of educational resources](https://indico.desy.de/event/28202/contributions/106197/) | EPS-HEP 2021 | 28 July 2021 | Meirin Oan Evans |
| [A proposal for Open Access data and tools multi-user deployment using ATLAS Open Data for Education](https://indico.cern.ch/event/948465/contributions/4323945/) | vCHEP 2021 | 20 May 2021 | Arturo Sánchez Pineda, Giovanni Guerrieri |
| [ATLAS Open Data at 13TeV - The journey to a fully educational HEP dataset](https://indico.cern.ch/event/868940/contributions/3814036/) | ICHEP 2020 | 29 July 2020 | Kate Shaw |
| [ATLAS OpenData and OpenKey: using low tech computational tools for students training in High Energy Physics](https://indico.cern.ch/event/587955/contributions/2938491/) | CHEP 2018 | 12 July 2018 | Arturo Sánchez Pineda |
| [ATLAS Open Data project](https://indico.cern.ch/event/686555/contributions/2971025/) | ICHEP 2018 | 6 July 2018 | Meirin Oan Evans |

## Posters at conferences
| Title		       | Conference      | Date       | Author       |
|------------------|-----------------|------------|--------------|
| [Open Data at ATLAS: Bringing TeV collisions to the World](https://indico.cern.ch/event/1291157/contributions/5885392/) | ICHEP 2024 | 19 July 2024 | Mariana Isabel Vivas Albornoz |
| [Engaging Universities and Beyond: Open Data for Education and Research](https://indico.cern.ch/event/1198609/contributions/5340376/) | LHCP 2023 | 23 May 2023 | Leonardo Toffolin |
| [ATLAS Open Data: developing education and outreach resources from research data](https://agenda.infn.it/event/28874/contributions/176209/) | ICHEP 2022 | 8 July 2022 | Meirin Oan Evans |
| [ATLAS Open Data: developing education and outreach resources from research data](https://indico.cern.ch/event/1109611/contributions/4821267/) | LHCP 2022 | 17 May 2022 | Ricardo Jose Morais Silva Gonçalo |
| [ATLAS Open Data: Data visualisation and educational physics analysis to re-discover the Higgs](https://indico.cern.ch/event/856696/contributions/3856041/) | LHCP 2020 | 28 May 2020 | Meirin Oan Evans |
| [The ATLAS Open Data project](https://indico.cern.ch/event/681549/contributions/2956282/) | LHCP 2018 | 5 June 2018 | Caterina Doglioni |

