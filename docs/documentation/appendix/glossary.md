# Glossary

This is not intended to be an exhaustive glossary.  You will find answers to many of your questions by using a search engine. However, here are a few ATLAS physics terms explained to start you off. If what you are looking for is not here, please check the [ATLAS glossary](https://atlas.cern/glossary).

| Term | Definition |
|------|------------|
|Pseudorapidity (&eta;)| Describes the angle of a particle relative to the beam axis. |
| Azimuth angle (&phi;) | Describes the angle of a particle measured from the x-axis, around the beam. |
| Event displays | Are like snap-shots, tracing the paths of particles produced in a collision.
| Branching fraction | In general, a particle can decay in several modes or decay channels. For example, a Z boson can decay into a pair of neutrinos, a pair of charged leptons, or a pair of quarks (i.e., all the standard model fermions lighter than half the Z mass). The probability for a Z to decay into a neutrino pair is about 20%, into a pair of charged leptons (electrons, muons, or taus) is about 10%, and into a pair of quarks (u,d,c,s,b) is about 70%. These probabilities are called branching fractions. |
| Channel | The decay channel signifies a certain route a physics process has taken. For example, the W boson may decay to either a pair of hadrons or a pair of leptons. The signature for these W bosons in the detector are therefore either two hadrons or two leptons.|
| Electron Volt (eV) | One electron volt is equal to ~1.6 x 10<sup>-19</sup> joules. MeV (10<sup>6</sup>) and TeV (10<sup>12</sup>) are units of energy used in particle physics. 1 TeV is about the energy of motion of a flying mosquito.|
| Antiparticle | Subatomic particle having the same mass as one of the particles of ordinary matter but opposite electric charge and magnetic moment.|
| Pile-up | Average number of particle interactions per bunch-crossing.|