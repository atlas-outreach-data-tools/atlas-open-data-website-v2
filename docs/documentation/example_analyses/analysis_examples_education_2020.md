# Example Analyses with the 13 TeV Data for Education
The general aim of the ATLAS Open Data and tools released is to provide a straightforward interface to replicate the procedures used by high-energy-physics researchers and enable users to experience the analysis of particle physics data in educational environments. Therefore, it is of significant interest to check the correct modelling of several SM process by the 13 TeV ATLAS Open Data MC simulation.

Hence, **twelve examples of physics analysis** (as reported in the [official release document](https://cds.cern.ch/record/2707171)) using the 13 TeV ATLAS Open Data inspired by and following as closely as possible the procedures and selections taken in already published ATLAS Collaboration physics results are introduced:

+ **Four high statistics** analyses with a selection of:
  * W-boson leptonic-decay events,
  * single-Z-boson events, where the Z boson decays into an electron–positron or muon–antimuon pair,
  * single-Z-boson events, where the Z boson decays into a tau-lepton pair with a hadronically decaying tau-lepton accompanied by a tau-lepton that decays leptonically,
  * top-quark pairs in the single-lepton final state.

Each of these analyses have sufficiently high event yields to study the SM processes in detail, and are intended to show the general good agreement between the released 13 TeV data and MC prediction. They also enable the study of SM observables, such as the mass of the W and Z bosons, and that of the top quark.

+ **Three low statistics** analyses with a selection of single top-quarks produced in the single-lepton t-channel, diboson WZ events produced in the tri-lepton final state and diboson ZZ events produced in the fully-leptonic final states. These analyses illustrate the statistical limitations of the released dataset given the low production cross-section of the rare processes, where the variations between data and MC prediction are attributed to sizeable statistical fluctuations.

+ **Three SM Higgs boson** analyses with a selection of events in the H &rarr; WW, H &rarr; ZZ and H &rarr; &gamma;&gamma; decay channels, which serve as examples to implement simplified analyses in different final-state scenarios and "re-discover" the production of the SM Higgs boson.

+ **Two BSM physics** analyses searching for new hypothetical particles: one implementing the selection criteria of a search for direct production of superpartners of SM leptons, and the second one implementing the selection criteria of a search for new heavy particles that decay into top-quark pairs, provided to implement a simplified analysis for searching for new physics using different physics objects.

For an introduction to the Higgs boson, visit the [Higgs boson introduction section](https://home.cern/).

## Example of Physics Analysis

<details>
<summary> <h3>SM W-boson production in the single-lepton final state</h3></summary>

W-bosons are produced abundantly at the LHC and the measurements of the inclusive production cross section of the W bosons and of the asymmetry between the positively-charged and negatively-charged W-boson cross sections constitute important tests of the SM. In addition, W+jets processes are a significant background to studies of SM processes such as single-top production, top-quark pair production, as well as searches for the SM Higgs boson and for BSM physics.

In order to identify these events, one needs to apply the standard object-selection criteria (defined in "Reconstructed physics objects"), with a stricter lepton *p*<sub>T</sub> (> 35 GeV) and lepton calorimeter and tracking isolation (< 0.1) requirements and tight lepton identification criteria, and an event-selection criteria defined as:

* Single-electron or single-muon trigger satisfied.
* Exactly one light leptop (electron or muon) with *p*<sub>T</sub> > 35 GeV.
* Missing transverse momentum larger than 30 GeV.
* The transverse mass of the W-boson (*M*<sub>T</sub><sup>W</sup>): M<sub>T</sub><sup>W</sup> > 60 GeV.

At the end, one is able to compare data and MC prediction for the distribution of e.g. the missing transverse momentum, as seen below:

import sla from '../images/SL1_plot.png';

<div style={{textAlign: 'center'}}>
<img src={sla} alt="Missing transverse momentum" style={{width: 600}} />
</div>

</details>

<details>
<summary> <h3>T-channel single-top-quark production in the single-lepton final state</h3></summary>

At hadron colliders, top quarks are predominantly produced in pairs via the flavour conserving strong interaction, but single top-quark production can occur via charged current electroweak processes involving a Wtb vertex. At leading order (LO) in quantum chromodynamics (QCD) perturbation theory, three sub-processes contribute to single top-quark production: an exchange of a virtual W boson either in the
t-channel or in the s-channel, or the associated production of a top quark with an on-shell W boson (Wt).

In pp collisions, the t-channel exchange is the dominant production process of single top quarks: an exchange of a space-like W boson due to the interaction of a light quark with a b-quark produces a top quark and a forward light-quark (called the spectator quark) in the final state.

In order to identify these events, one needs to apply the standard object-selection criteria (defined in "Reconstructed physics objects"), with a stricter lepton *p*<sub>T</sub> (> 35 GeV) and lepton calorimeter and tracking isolation (< 0.1) requirements and tight lepton identification criteria, and an event-selection criteria defined as:

* Single-electron or single-muon trigger satisfied.
* Exactly one lepton (electron or muon) with p<sub>T</sub>35 GeV.
* Missing transverse momentum *E*<sub>T</sub><sup>miss</sup> larger than 30 GeV.
* Transverse mass of the W-boson *M*<sub>T</sub><sup>W</sup> larger than 60 GeV.
* Exactly two jet with *p*<sub>T</sub> > 30 GeV, with exactly one of then *b*-tagged (MV2c10 at 70% WP).
* The pseudorapidity of the untagged jet must satisfy |&eta;| > 1.5.
* The separation in &eta; between the untagged jet and the *b*-tagged jet must be larger that 1.5.
* The scalar sum (*H*<sub>T</sub>) of the *p*<sub>T</sub> of the lepton, the *p*<sub>T</sub> of the jets and *E*<sub>T</sub><sup>miss</sup> must be larger than 195 GeV.
* The mass of the lepton and the *b*-tagged jet, *m*<sub>lb</sub>, must be larger than 150 GeV.

At the end, one is able to compare data and MC prediction for the distribution of e.g. the mass of the lepton and the b-tagged jet, as seen below:

import slb from '../images/SL2_plot.png';

<div style={{textAlign: 'center'}}>
<img src={slb} alt="Mass of the lepton and the b-tagged jet" style={{width: 600}} />
</div>

</details>

<details>
<summary> <h3>Top-quark pair production in the single-lepton final state</h3></summary>

The top quark is the heaviest elementary particle in the SM, with a mass mt of around 172.5 GeV, which is close to the electroweak symmetry breaking scale. At the LHC, top quarks are primarily produced in quark–antiquark pairs (tt), and due to its large production cross section ( around 830 pb at 13 TeV), the LHC can be viewed as "a top-quark factory". Top quarks have a rich phenomenology which includes high-p<sub>T</sub> jets, b-jets, leptons and missing transverse momentum. Its understanding is crucial for studying rarer processes, given that tt production is a background to virtually all processes having leptons and multiple jets in their final states.

In order to identify these events, one needs to apply the standard object-selection criteria (defined in "Reconstructed physics objects"), with a stricter lepton p<sub>T</sub> requirement and tight lepton identification criteria, and an event-selection criteria defined as:

* Single-electron or single-muon trigger satisfied.
* Exactly one lepton (electron or muon) with *p*<sub>T</sub> > 30 GeV.
* Missing transverse momentum *E*<sub>T</sub><sup>miss</sup> larger than 30 GeV.
* Transverse mass of the W-boson *M*<sub>T</sub><sup>W</sup> larger than 30 GeV.
* At least four jets with *p*<sub>T</sub> > 30 GeV, out of which at least two are *b*-tagged (MV2c10 at 70% WP).

At the end, one is able to compare data and MC prediction for the distribution of e.g. the invariant mass of the three-jets combination with the highest vector *p*<sub>T</sub>, as seen below:

import slc from '../images/SL3_plot.png';

<div style={{textAlign: 'center'}}>
<img src={slc} alt="Invariant mass of the three-jets combination with the highest vector pT" style={{width: 600}} />
</div>

</details>

<details>
<summary> <h3>SM Z-boson production in the two-lepton final state</h3></summary>

The study of Z-boson production in pp collisions provides a stringent test of perturbative QCD. In addition, the SM Z-boson process, often produced in association with one or more jets, is a significant background to searches for the SM Higgs boson and for physics beyond the SM.

In order to identify these events, one needs to apply the standard object-selection criteria (defined in "Reconstructed physics objects"), and leptons are required to pass tight identification criteria, and an event-selection criteria defined as:

* Single-electron or single-muon trigger satisfied.
* Exactly two same-flavour opposite-charge (SFOS) leptons (electrons or muons) with *p*<sub>T</sub> > 25 GeV.
* Dilepton invariant mass is required to be within: 66 < *m*<sub>&ell;&ell;</sub> < 116 GeV.

At the end, one is able to compare data and MC prediction for the distribution of e.g. the di-muon invariant mass, as seen below:

import dla from '../images/fig_04h.png';

<div style={{textAlign: 'center'}}>
<img src={dla} alt="Di-muon invariant mass" style={{width: 600}} />
</div>

</details>

<details>
<summary> <h3>SM Higgs boson production in the H &rarr; WW decay channel in the two-lepton final state</h3></summary>

As described in the "[Brief introduction to the Higgs Boson](https://home.cern/)", the H &rarr; WW decay branching ratio for the Higgs boson with a mass of 125 GeV is predicted to be 0.214 in the SM, and corresponds to the second-largest branching fraction after the dominant H &rarr; bb decay mode.
The predicted Higgs-boson production cross sections via the dominant gluon–gluon fusion (ggF) and vector-boson fusion (VBF) times H &rarr; WW branching fraction are 10.4 pb and 0.81 pb for ggF and VBF, respectively. Reducing the numerous backgrounds contributing to this channel and accurately estimating the remainder is a major challenge in this analysis. The dominant background stems from non-resonant WW diboson
production, while tt, single-top-quark and W+jets (with the jet misidentified as a lepton) events, as well as non-resonant WZ and ZZ processes contribute to the overall background.

In order to identify these events, one needs to apply the standard object-selection criteria (defined in "Reconstructed physics objects") with a stricter lepton calorimeter and tracking isolation (< 0.1) requirements and tight lepton identification criteria, and an event-selection criteria defined as:

* Single-electron or single-muon trigger satisfied.
* Exactly two isolated, different-flavour opposite-sign leptons (electrons or muons) with *p*<sub>T</sub> > 22 and 15 GeV, respectively.
* Missing transverse momentum *E*<sub>T</sub><sup>miss</sup> larger than 30 GeV.
* Exactly zero or at most one jet with *p*<sub>T</sub> > 30 GeV, and exactly zero *b*-tagged jets (MV2c10 at 85% WP) with *p*<sub>T</sub> > 20 GeV.
* Azimuthal angle between *E*<sub>T</sub><sup>miss</sup> and the dilepton system &Delta;&phi;(&ell;&ell;,*E*<sub>T</sub><sup>miss</sup>) > &pi;/2.

At the end, one is able to compare data and MC prediction for the distribution of e.g. the dilepton transverse mass, as seen below. A small excess in data is observed, and which corresponds to the production of the SM Higgs boson.

import dlb from '../images/fig_05h.png';

<div style={{textAlign: 'center'}}>
<img src={dlb} alt="Dilepton transverse mass" style={{width: 600}} />
</div>

</details>

<details>
<summary> <h3>Search for supersymmetric particles in the two-lepton final state</h3></summary>

Supersymmetry (SUSY) is one of the most studied extensions of the SM. In its minimal form it predicts new fermionic (bosonic) partners of the fundamental SM bosons (fermions) and an additional Higgs doublet. These new SUSY particles, or "sparticles", can provide an elegant solution to the gauge hierarchy problem and provide a natural candidate for dark matter in cases where the lightest supersymmetric particle is a so called weekly interacting massive particle.

In the following, we are going to focus on a search for direct production of pairs of sleptons, the superpartners of the SM leptons, where each slepton decays directly into the lightest neutralino and the corresponding lepton. A simplified benchmark model is chosen for this
search, in which the mass of the slepton and the neutralino are the only free parameters. The predicted cross section for slepton production, each with a mass of 600 GeV, and neutralino mass of 300 GeV is 0.7 fb.

In order to search for these events, one needs to apply the standard object-selection criteria (defined in "Reconstructed physics objects") with a loose lepton p<sub>T</sub> and loose lepton identification criteria, and an event-selection criteria defined as:

* Single-electron or single-muon trigger satisfied.
* Exactly two SFOS leptons (electrons or muons) with *p*<sub>T</sub> > 25 and 20 GeV, respectively.
* Dilepton invariant mass, *m*<sub>&ell;&ell;</sub>, is required to be larger than 40 GeV.
* Two signal regions (SR) are constructed: loose and tight; both SR are required to have exactly zero *b*-tagged jets (MV2c10 at 77% WP) with *p*<sub>T</sub> > 20 GeV and exactly zero non-b-tagged jets with *p*<sub>T</sub> > 60 GeV.
* Additional cuts on the transverse mass variable, *m*<sub>T2</sub>, and dilepton invariant mass, *m*<sub>&ell;&ell;</sub>, are applied in the loose SR (*m*<sub>&ell;&ell;</sub> > 111 GeV and *m*<sub>T2</sub> > 100 GeV) and tight SR (*m*<sub>&ell;&ell;</sub> > 300 GeV and *m*<sub>T2</sub> > 130 GeV).

At the end, one is able to compare data and MC prediction for the distribution of e.g. the missing transverse momentum, as seen below. Interesting to note are the events observed in data at very high values (> 900 GeV). These have been investigated further and found to contain very high-p<sub>T</sub> calorimeter-tagged muons which passes the loose muon selection working point. By applying the tight identification criteria on the muons, these events are rejected.

import dlc from '../images/fig_06f.png';

<div style={{textAlign: 'center'}}>
<img src={dlc} alt="Missing transverse momentum" style={{width: 600}} />
</div>

</details>

<details>
<summary> <h3>SM WZ diboson production in the three-lepton final state</h3></summary>

The study of the diboson production is an important part of the physics programme in hadron collisions as it represents an important test of the electroweak sector of the SM. In particular, the WZ diboson production arises from two vector bosons radiated by quarks or from the decay of a virtual W boson into a WZ pair, the latter of which involves a triple gauge coupling.

In order to identify these events, one needs to apply the standard object-selection criteria (defined in "Reconstructed physics objects") with a loose lepton p<sub>T</sub> requirement and tight lepton identification criteria, and an event-selection criteria defined as:

* Single-electron or single-muon trigger satisfied.
* Exactly three leptons (electrons or muons) with *p*<sub>T</sub> > 20 GeV, at least one of them should have *p*<sub>T</sub> > 25 GeV.
* Candidate events are required to have at least one SFOS-pair of leptons with an invariant mass that is consistent with the nominal Z-boson mass (*m*<sub>Z</sub> = 91.18 GeV) to within 10 GeV, considered to be the Z-boson candidate.
* Both the missing transverse momentum *E*<sub>T</sub><sup>miss</sup> and the transverse mass of the W candidate *M*<sub>T</sub><sup>W</sup> are required to be larger than 30 GeV.

At the end, one is able to compare data and MC prediction for the distribution of e.g. the invariant mass of the reconstructed Z-boson candidate, as seen below.

import tla from '../images/fig_07g.png';

<div style={{textAlign: 'center'}}>
<img src={tla} alt="Invariant mass of the reconstructed Z-boson candidate" style={{width: 600}} />
</div>

</details>

<details>
<summary> <h3>SM ZZ diboson production in the four-lepton final state</h3></summary>

The study of ZZ diboson production in pp interactions at the LHC not only can be used as a test of the electroweak sector of the SM. The SM ZZ production can proceed via a SM Higgs boson propagator, although this contribution is suppressed in the region where both Z bosons are produced on-shell. Hence, non-resonant ZZ diboson production is an important background for searches of the SM Higgs boson with
its subsequent decay to ZZ.

In order to identify these events, one needs to apply the standard object-selection criteria (defined in "Reconstructed physics objects") with a loose lepton p<sub>T</sub> requirement and tight lepton identification criteria, and an event-selection criteria defined as:

* Single-electron or single-muon trigger satisfied.
* Exactly four leptons (electrons or muons) with *p*<sub>T</sub> > 20 GeV, at least one of them should have *p*<sub>T</sub> > 25 GeV.
* Two pairs of SFOS leptons (&mu;<sup>+</sup>&mu;<sup>-</sup> or *e*<sup>+</sup>*e*<sup>-</sup>) are formed, giving rise to the three channels: 4*e*, 4&mu; and 2*e*2&mu;.
* Each SFOS lepton pair must have an invariant mass within a range of 66 < *m*<sub>&ell;&ell;</sub> < 116 GeV.
* In the 4*e* and 4&mu; channels, where there are two possible ways to form SFOS lepton pairs, the combination that minimises |*m*<sub>&ell;&ell;,1</sub> - *m*<sub>Z</sub>| + |*m*<sub>&ell;&ell;,2</sub> - *m*<sub>Z</sub>| is chosen, where *m*<sub>&ell;&ell;,1</sub> and *m*<sub>&ell;&ell;,2</sub> are the invariant masses of the two lepton pais.

At the end, one is able to compare data and MC prediction for the distribution of e.g. the transverse momentum of the four-lepton system, as seen below.

import fla from '../images/fig_08g.png';

<div style={{textAlign: 'center'}}>
<img src={fla} alt="Transverse momentum of the four-lepton system" style={{width: 600}} />
</div>

</details>

<details>
<summary> <h3>SM Higgs boson production in the H &rarr; ZZ decay channel in the four-lepton final state</h3></summary>

The search for the SM Higgs boson through the decay H &rarr;  ZZ &rarr; 4l, where l = e or &mu;, represents the so called "golden channel" and leads to a narrow four-lepton invariant-mass peak on top a relatively smooth and small background, largely due to the excellent momentum resolution of the ATLAS detector. The Higgs-boson decay branching ratio to the four-lepton final state for the Higgs boson mass of 125 GeV is predicted
to be 0.0124% in the SM, and the expected cross section times branching ratio for the process H &rarr; ZZ &rarr; 4l is 2.9 fb at 13 TeV.
Hence, based on an integrated luminosity of the current ATLAS Open Data set of 10/fb, one expects a total of 29 events to have been produced in the four-lepton final state (before reconstruction and event selection).

In order to identify these events, one needs to apply the standard object-selection criteria (defined in "Reconstructed physics objects") with a loose lepton p<sub>T</sub> requirement and loose lepton calorimeter- and track-based isolation requirements, and an event-selection criteria defined as:

* Single-electron or single-muon trigger satisfied.
* Exactly four leptons (electrons or muons) with *p*<sub>T</sub> > 25, 15, 10, 7 GeV, respectively.
* Higgs-boson candidates are formed by selecting two SFOS lepton pairs.
* The leading pair is defined as the SFOS pair with the mass *m*<sub>&ell;&ell;,1</sub> closest to the Z boson mas *m*<sub>Z</sub>, and the subleading pair is defined as the SFOS pair with the mass *m*<sub>&ell;&ell;,1</sub> second closest to *m*<sub>Z</sub>.

At the end, one is able to compare data and MC prediction for the distribution of e.g. the four-lepton invariant mass, as seen below. An excess of events in the distribution of the four-lepton invariant mass near 125 GeV is observed, which corresponds to the expected in the SM production of the Higgs boson.

import flb from '../images/fig_09h.png';

<div style={{textAlign: 'center'}}>
<img src={flb} alt="Four-lepton invariant mass" style={{width: 600}} />
</div>

</details>

<details>
<summary> <h3>SM Z-boson production in the two-tau-lepton final state</h3></summary>

The &tau;-leptons play an important role in the physics programme of the LHC. They serve not only as a
foundation to identify and precisely measure several SM production processes such as W &rarr;  &tau;&nu; and Z &rarr;  &tau;&tau;, which represent the main irreducible backgrounds in measurements of the Higgs boson in H &rarr;  &tau;&tau; final states, but also are widely used in searches for new physics beyond the SM.

In the following, a selection criteria is made in order to reconstruct the Z &rarr;  &tau;&tau; decays from a hadronically decaying &tau;-lepton, accompanied by a &tau-lepton that decays leptonically. The leptonic &tau-lepton decays are reconstructed as electrons and muons in the final state. The hadronic &tau-lepton decays produce a highly collimated jet in the detector consisting of an odd number of charged hadrons and possibly additional calorimetric energy deposits from neutral decay products.

In order to identify these events, one needs to apply the standard object-selection criteria (defined in "Reconstructed physics objects") with a stricter lepton p<sub>T</sub> and lepton calorimeter and tracking isolation (< 0.1) requirements and tight lepton identification criteria, and an event-selection criteria defined as:

* Single-electron or single-muon trigger satisfied.
* Exactly one light lepton (electron or muon) with *p*<sub>T</sub> > 25 GeV.
* The two leptons are required to be of opposite charge.
* The transverse mass calculated for the *E*<sub>T</sub><sup>miss</sup> and the momentum of the selected light lepton is required to have *M*<sub>T</sub><sup>W</sup> < 30 GeV.
* The sum of the azuimuthal angular separation between the &tau;<sub>h</sub> candidate and the *E*<sub>T</sub><sup>miss</sup>  directions, and between the lepton and the *E*<sub>T</sub><sup>miss</sup>  direction is required to be less than 3.5.
* The visible mass of the &tau;<sub>h</sub> candidate and lepton, *m*<sub>vis</sub> = *m*(&tau;<sub>h</sub>,lepton), is required to satisfy 35 < *m*<sub>vis</sub> < 75 GeV.

At the end, one is able to compare data and MC prediction for the distribution of e.g. the reconstructed di-tau invariant mass, as seen below.

import tt from '../images/fig_10h.png';

<div style={{textAlign: 'center'}}>
<img src={tt} alt="Reconstructed di-tau invariant mass" style={{width: 600}} />
</div>

</details>

<details>
<summary> <h3>Search for BSM Z' &rarr; tt in the single-lepton boosted final state</h3></summary>

Despite the spectacular phenomenological and experimental success of the SM, searches for new physics phenomena at the LHC are constantly ongoing. As an example, with a mass close to the scale of electroweak symmetry breaking, the top quark, besides having a large coupling to the SM Higgs boson, is predicted to have large couplings to new particles hypothesised in many BSM models.

In the following, we focus on implementing the selection criteria of a search for new heavy particles that decay into top-quark pairs in events containing a single charged lepton, large-R jets and missing transverse momentum. A particular benchmark model chosen for this search produces a new gauge boson Z' with a mass of 1 TeV and width of 10 GeV that decays into a tt-pair

In order to identify these events, one needs to apply the standard object-selection criteria (defined in "Reconstructed physics objects"), with a stricter lepton p<sub>T</sub> requirement and tight lepton identification criteria, and an event-selection criteria defined as:

* Single-electron or single-muon trigger satisfied.
* Exactly one lepton (electron or muon) with p<sub>T</sub>35 GeV.
* *E*<sub>T</sub><sup>miss</sup> larger than 20 GeV and *E*<sub>T</sub><sup>miss</sup> + *M*<sub>T</sub><sup>W</sup> > 60 GeV.
* At least one small-R jet close to the lepton i.e. with &Delta;R(small-R jet, lepton) < 2.0.
* Exactly one large-R jet, passing the simplified top-tagging requirements: a mass larger than 100 GeV and N-subjettiness ratio &tau;<sub>23</sub> < 0.75.
* This *top-tagged* large-T jet must be well separated from the lepton, &Delta;&phi;(large-R jet, lepton) > 1.0, and from the small-R jet associated with the lepton, &Delta;R(large-R jet, small-R jet) > 1.5.
* At least one *b*-tagged (MC2c10 at 70% WP) small R-jet that fulfils the following requirements: it is either inside the top-tagged large-R jet, &Delta;R(large-R jet, *b*-tagged jet)< 1.0, or it is the small-R jet associated with the lepton.

At the end, one is able to compare data and MC prediction for the distribution of e.g. the approximate mass of the tt system, as seen below. The benchmark 1 TeV Z' model is overlaid, clearly showing the different kinematic properties of this particular BSM prediction.

import slbb from '../images/fig_11h.png';

<div style={{textAlign: 'center'}}>
<img src={slbb} alt="Approximate mass of the tt system" style={{width: 600}} />
</div>

</details>

<details>
<summary> <h3>SM Higgs boson production in the H &rarr; yy decay channel in the two-photon final state</h3></summary>

he H &rarr; yy decay mode provides a very clear and distinctive signature of two isolated and highly energetic photons, and is one of the main channels studied at the LHC. Despite the small branching ratio, a reasonably large signal yield can be obtained thanks to the high photon reconstruction and identification efficiency at the ATLAS experiment. Furthermore, due to the excellent photon energy resolution of the ATLAS calorimeter, the signal manifests itself as a narrow peak in the diphoton invariant mass spectrum on top of a smoothly falling irreducible background from QCD production of two photons.

In order to identify these events, one needs to apply the standard object-selection criteria (defined in "Reconstructed physics objects"), and an event-selection criteria defined as:

* Diphoton trigger satisfied.
* Exactly two photons with *E*<sub>T</sub> 35 and 25 GeV, respectively.
* Leading and subleading photon candidate are respectively required to have *E*<sub>T</sub>/*m*<sub>&gamma;&gamma;</sub> > 0.35 and 0.25.
* Diphoton invariant mass *m*<sub>&gamma;&gamma;</sub> between 105 GeV and 160 GeV.

At the end, one is able to compare data and MC prediction for the distribution of e.g. the diphoton invariant-mass spectrum, as seen below. An excess of events in the distribution of the four-lepton invariant mass near 125 GeV is observed, which corresponds to the expected in the SM production of the Higgs boson.

import yy from '../images/fig_12b.png';

<div style={{textAlign: 'center'}}>
<img src={yy} alt="Diphoton invariant-mass spectrum" style={{width: 600}} />
</div>

</details>