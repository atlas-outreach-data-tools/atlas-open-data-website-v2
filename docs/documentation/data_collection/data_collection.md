# ATLAS Data Collection
The ATLAS experiment at the Large Hadron Collider (LHC) is one of the most ambitious scientific experiments of our time, aiming to understand the fundamental particles and forces that shape our universe. The process of data collection, capturing the interactions of proton-proton (pp) and ion-ion collisions at very high energies, and analysis, understanding these collisions, is vital for this search. This section outlines how the ATLAS experiment works: how the experiment runs and how the collision events are processed for reconstruction, which is part of the process to eventually offer insight into the Standard Model and beyond.

## ATLAS Runs and LHC Operations
An ATLAS run is a coordinated data adquisition effort that coincides with the LHC fill cycle. It last tipically around 12 hours, capturing very precise collision data for high-energy physics research. Not to confuse with multi-year "Runs", these "runs" are shorter periods of time that are part of the general operational cycles of the ATLAS experiment, and more generally, of the LHC.

During ATLAS runs, the detector operates in sync with the LHC fill cycle, which involves the stages of beam injection, ramp-up to high energies, beam focusing, and the stable beams phase, where data from collisions are recorded.

- **Injection**: The magnets are charged to appropriate levels, and beams are injected into the LHC rings following a specified filling scheme, detailing proton bunch numbers and their spacings.
- **Ramp**: Acceleration of the particles until the desired collision energy occurs, involving both the radio frequency systems and increasing magnet currents.
- **Squeeze and Adjust**: Beam sizes at the interaction points are reduced (squeeze), followed by adjustments for optimal collisions (adjust).
- **Stable Beams**: This phase marks when collisions occur, and it is safe to record data.
- **Dump and Ramp Down**: Beams are safely extracted and discarded, and magnetic fields are reduced.

## Collisions at ATLAS
The LHC features 3564 bunch crossings per revolution, with each crossing containing paired, unpaired, or empty bunches. "Paired" refers to crossings where both beams have bunches, "unpaired" where only one beam has a bunch, and "empty" where no bunches are present. These are organized into bunch groups, defined per fill and used with trigger conditions. Proton or ion beams are accelerated and collided within the ATLAS detector, producing new particles which are detectable as result of the collision. These so called "events" occur over a billion times per second. 

The reconstruction of these events is influenced by "pile-up," which refers to multiple inelastic pp collisions in a single bunch crossing and neighboring bunch crossings.

## Event Reconstruction and Luminosity
Each collision event is full of data, with multiple interaction vertices produced along the beam line. The primary vertex, defined by the largest sum of the squares of the transverse momenta of associated tracks, is crucial for identifying various particles and searching for new physics phenomena.

![](../images/zpileup_alltracks_withcaption.png)

## Luminosity Blocks and Event Selection
Data acquisition in an ATLAS run is segmented into Luminosity Blocks (LBs), which are fundamental units evaluated for physics analysis. Events are selected based on criteria such as the number of vertices and the presence of high-transverse-momentum particles, ensuring the highest data quality for analysis. The events are then organized into a [Good Run List](./GRL_definition.md), explained in the next section.

## Multi-Year Operational Periods
"Run 1" through "Run 4" refer to the extended operational periods of the LHC and ATLAS, each marked by different energy levels, luminosities, and advancements. These periods are vital for discoveries like the Higgs boson and for delving into the laws of the universe.

- **Run 1 (2009-2013)**: The inaugural operational phase of the LHC, including the discovery of the Higgs boson. Initial energies were up to 7 TeV per beam, later increased to 8 TeV.
- **Run 2 (2015-2018)**: Characterized by an increase in collision energy to 13 TeV and improvements in luminosity, enabling deeper studies of the Higgs boson and searches for new particles.
- **Run 3 (2022-present)**: Marks further increases in LHC's luminosity and advancements in detector and data processing technology, focusing on detailed Higgs boson studies and exploring previous anomalies.
- **Run 4 (late-2020s)**: Expected to initiate the High-Luminosity LHC (HL-LHC) project, significantly enhancing luminosity and data volume, paving the way for more precise measurements and potential new discoveries.
