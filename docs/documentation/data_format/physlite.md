# PHYSLITE
The [PHYSLITE format](https://cds.cern.ch/record/2870350/files/ATL-SOFT-PROC-2023-029.pdf) represents an advancement in ATLAS data analysis, transitioning from [Run 2](../data_collection/data_collection.md#multi-year-operational-periods) through to [Run 4](../data_collection/data_collection.md#multi-year-operational-periods). It is designed to efficiently manage and analyze the large datasets generated in the ATLAS experiment and stands out for its streamlined approach.

Developed during [Run 3](../data_collection/data_collection.md#multi-year-operational-periods), PHYSLITE incorporates calibrated objects and selections with lower transverse momentum (pT) thresholds, coupled with elements like machine learning scores. This format not only minimizes the need for storing extensive calibration data but also reduces CPU usage, optimizing the overall analysis workflow.

## Key Features of PHYSLITE
* **Reduced File Size**: PHYSLITE targets a file size of 10 kB per event for data and 12 kB for MC, a significant reduction compared to previous formats.
* **CPU Efficiency**: Preliminary evaluations show a 25% reduction in CPU usage compared to previous models.
* **Unskimmed and Monolithic**: PHYSLITE is designed to be a one-size-fits-all solution, fitting various use cases without the need for multiple versions.
* **Direct Analysis Capability**: PHYSLITE can be analyzed directly, avoiding the need for creating flat n-tuples and further reducing storage demands.

## Enhanced Accessibility and Open Data in PHYSLITE
PHYSLITE not only significantly reduces storage needs but also marks a leap forward in making high-energy physics data more accessible. This accessibility is crucial for the diverse community of researchers, students, and people interested in exploring the ATLAS experiment's open data.

* **Understanding PHYSLITE Data**: To help with the understanding and use of PHYSLITE, a [description of the variables](https://atlas-physlite-content-opendata.web.cern.ch/) is provided. This resource explains the data structure and elements within PHYSLITE.
* **Hands-On Learning with PHYSLITE**: A practical approach to learning is available through a [PHYSLITE tutorial notebook](../../tutresearch/physlitetut). This tutorial guides you through the process of using PHYSLITE files in Python, from basic operations to more advanced analysis. It's a good tool for anyone looking to get hands-on experience with PHYSLITE data.
* **Writting NTuples**: To transform the PHYSLITE files into a NTuple using C++, you can follow the section [Writing NTuples and Trees using the Text Configuration](https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/cpalg_ntuple/) from the [ATLAS Analysis Software Tutorial](https://atlassoftwaredocs.web.cern.ch/ASWTutorial/).
