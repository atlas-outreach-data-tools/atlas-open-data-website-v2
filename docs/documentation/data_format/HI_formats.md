# Heavy Ion Data Formats
In the ATLAS experiment, heavy ion collisions offer a opportunity to study the behavior of nuclear matter under extreme conditions. To facilitate research in this area, we have developed specialized data formats for heavy ion physics analyses. These formats are designed to be lighter than traditional heavy ion data formats, optimizing both storage and computational efficiency.

Currently, we provide data formats for:
- Minimum Bias
- Hard Probes (to be added in the future)

## Minimum Bias Data Format
The Minimum Bias data format is crafted to support analyses that require an unbiased and comprehensive sample of collision events. This format is used for studies like flow analysis, which investigates collective motion and the properties of the quark-gluon plasma.

### Key Features
- **Detailed Tracking Information**: Includes full tracking data, capturing all reconstructed tracks necessary for flow analyses.
- **Complete Particle Information in MC**: In Monte Carlo (MC) simulations, all particles are retained.
- **Optimized Data Sizes**:
    - **Data**: Approximately 16.852 kB per event.
    - **MC**: Approximately 296.150 kB per event.
- **Efficient Analysis Workflow**: Despite the comprehensive information included, the format is optimized for efficiency, enabling direct analysis without extensive data reduction steps.

### Resources and Accessibility
- **Variable Descriptions**: A [description of the variables](https://atlas-physlite-content-opendata.web.cern.ch) is available to help users understand the data structure and contents.
- **Analysis Tutorials**: Users can refer to the [PHYSLITE tutorial notebook](../../tutresearch/physlitetut) for guidance on using the data format in Python. While the tutorial is based on PHYSLITE, many of the techniques are applicable to the minimum bias format.
- **Writing NTuples**: To convert the data into NTuples using C++, follow the instructions in Writing NTuples and Trees using the [Text Configuration](https://atlassoftwaredocs.web.cern.ch/AnalysisSWTutorial/cpalg_ntuple/) from the [ATLAS Analysis Software Tutorial](https://atlassoftwaredocs.web.cern.ch/ASWTutorial/).