# Introduction to Heavy-Ion Physics
Heavy-ion physics is a field dedicated to exploring some of the most fundamental questions in physics: How does matter behave under extreme conditions? What are the properties of the strong force that binds the building blocks of matter together? By colliding heavy ions at ultra-relativistic speeds, we recreate conditions of immense temperature and density, similar to those that existed microseconds after the Big Bang. These collisions allow for the study of Quantum Chromodynamics (QCD), providing insights into the behavior of quarks and gluons — the fundamental constituents of matter.

In these high-energy collisions, normal nuclear matter can transform into quark-gluon plasma (QGP), a state where quarks and gluons are no longer confined within protons and neutrons. Studying the formation and properties of the QGP enables us to understand the strong interaction under conditions that cannot be achieved in other ways.

## Quantum Chromodynamics (QCD): The Strong Force Unveiled
Quantum Chromodynamics (QCD) is the theory that describes the strong force, one of the four fundamental forces in nature alongside gravity, electromagnetism, and the weak force. The strong force governs the interactions between quarks and gluons, which make up protons, neutrons, and other hadrons. Understanding QCD is important because:

1. **Mass Generation**: Most of the mass of visible matter arises not from the Higgs mechanism but from the strong interactions among quarks and gluons. The dynamic generation of mass through these interactions accounts for about 99% of the mass of protons and neutrons.
2. **Binding of Matter**: The strong force is responsible for binding quarks into protons and neutrons and holding nuclei together. However, describing nuclear structure directly from QCD remains one of the greatest challenges in theoretical physics.
3. **Probing Fundamental Physics**: High-energy colliders use QCD both as a subject of study and as a tool to explore other aspects of fundamental physics. Precise knowledge of QCD is critical for interpreting experimental results and searching for new physics beyond the Standard Model.

## Fundamental Properties of QCD
QCD is characterized by three unique properties that define the behavior of quarks and gluons:

#### 1. Confinement: The Binding of Quarks and Gluons
Quarks and gluons are never observed as free particles; they are confined within color-neutral hadrons such as protons and neutrons. This confinement occurs because the strong force between quarks increases with distance, preventing their separation. Confinement ensures the stability of ordinary matter under normal conditions.

#### 2. Asymptotic Freedom: The Strong Force Weakens at High Energies
As the energy or momentum transfer increases, the effective strength of the strong interaction decreases, allowing quarks to interact more freely at short distances.

import QCDfreedom from '../images/QCD_asymptotic.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={QCDfreedom} alt="Strong coupling vs. energy exchange (Q). Towards higher(lower) values of Q, the strong coupling decreases(increases)." style={{width: 650, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 1: Strong coupling vs. energy exchange (Q). Towards higher(lower) values of Q, the strong coupling decreases(increases).</figcaption>
</figure>

#### 3. Chiral Symmetry Breaking: Mass Generation
In a world with massless quarks, QCD would exhibit [chiral symmetry](https://en.wikipedia.org/wiki/Chirality_(physics)). However, this symmetry is broken both spontaneously by the QCD vacuum and explicitly by the small but non-zero quark masses. The breaking of chiral symmetry is a major source of hadron mass, arising from the strong interaction among quarks rather than directly from the Higgs mechanism.

## The QCD Phase Transition and the Quark-Gluon Plasma (QGP)
Under normal conditions, quarks and gluons are confined within hadrons, and chiral symmetry is broken. However, theoretical work since the 1970s suggests that by increasing the temperature and density of hadronic matter to extreme levels, it is possible to induce a phase transition in QCD. This transition moves matter from its normal confined state to a deconfined state known as the quark-gluon plasma (QGP), where quarks and gluons are no longer bound within hadrons.

import phasediagram from '../images/HI_phase_diagram.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={phasediagram} alt="Illustration of nuclear phase diagram." style={{width: 650, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 2: Illustration of nuclear phase diagram.</figcaption>
</figure>


### # Critical Conditions for the QCD Phase Transition
Calculations from [lattice QCD](https://en.wikipedia.org/wiki/Lattice_QCD) — a numerical approach to solving QCD equations on a discrete space-time grid — indicate that the QCD phase transition occurs at temperatures around $T_c≈150–160$ MeV, or over a trillion degrees Kelvin. Achieving such extreme conditions requires energies far beyond those encountered in typical environments.

#### Relevance of the QCD Phase Transition
For **cosmology**, studying the QCD phase transition offers insights into the early universe, which existed in a QGP state within the first few microseconds after the Big Bang. Understanding this transition helps explain how matter evolved to form protons, neutrons, and eventually atomic nuclei.

The QCD phase transition is also relevant in the study of **[neutron stars](https://en.wikipedia.org/wiki/Neutron_star)**. In the cores of neutron stars, densities are incredibly high, and the QCD phase transition may occur. Neutron stars could potentially contain regions of deconfined quark matter, making QCD relevant to astrophysical phenomena.

## Experimental Study of QCD: Heavy-Ion Collisions
To investigate QCD, we use heavy-ion collisions to momentarily create the temperatures and densities necessary for quark deconfinement.

By accelerating and colliding heavy nuclei like lead or gold at near-light speeds, experiments generate tiny fireballs with temperatures exceeding $10^{12}$ Kelvin. These conditions allow hadronic matter to transition into a QGP.

In addition to lead-lead collisions, the LHC is now colliding oxygen nuclei. Oxygen collisions provide insights into smaller systems and have connections to cosmic ray physics. For example, studying oxygen nuclei collisions helps us understand air showers generated by high-energy cosmic rays interacting with Earth’s atmosphere.

#### Mapping the QCD Phase Diagram
By varying the collision energy and the species of colliding nuclei, we can explore different regions of the QCD phase diagram, which maps the state of nuclear matter under varying temperatures and densities. This helps identify the conditions under which the QGP forms and understand the nature of the phase transition. In the image below you can see the region of exploration of the phase diagram by different experiments.

import expphasediagram from '../images/experiment_phase_diagram_HI.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={expphasediagram} alt="Experimental exploration of the phase diagram." style={{width: 650, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 3: Experimental exploration of the phase diagram.</figcaption>
</figure>

## Observables in Heavy-Ion Collisions
To characterize the QGP and understand the conditions of its formation, we rely on various observables measured in heavy-ion collisions. These observables provide insights into the geometry of the collision, the energy density achieved, and the properties of the produced matter.

### Centrality of Collisions: Geometry and Impact
Centrality quantifies the overlap region of the two colliding nuclei and is related to the collision's impact parameter — the transverse distance between the centers of the two nuclei at closest approach. Centrality is often expressed in percentages to indicate the degree of overlap. For example:
- *0–10%*: Highly central collisions with the greatest overlap, resembling extreme high-pileup events where all interactions occur in a concentrated region. These collisions generate maximal energy density and create the largest QGP volumes.
- *30–50%*: More peripheral collisions with less overlap and lower energy density. In these cases, the interaction dynamics begin to resemble those seen in proton-proton collisions.

The geometry of these collisions is used in determining experimental outcomes. Central collisions produce a larger volume of hot, dense matter, increasing the likelihood of QGP formation. By classifying events based on centrality, we can study how the initial geometry affects the evolution of the collision and the properties of the produced matter.

### Midrapidity Region: The Heart of the Collision
In high-energy heavy-ion collisions, the beams start with high rapidities. During the collision, the nuclei slow down to lower Lorentz factors ($\gamma$) and rapidities, depositing energy in the transparency regime. Most of the baryon number remains near the beam axis, while the midrapidity region, at the center of mass, becomes the hottest part of the system.

The midrapidity region is where particle production is maximal, and the highest temperatures are reached, making it the ideal region to search for signatures of the QGP. Heavy-ion collisions are also significantly more efficient at transferring energy from the beam rapidity region to midrapidity compared to proton-proton (pp) and proton-nucleus (pA) collisions, resulting in a steep increase in particle production per participant with energy. This efficiency creates the extreme conditions necessary to study the QGP.

## Particle Production and Spectra
Analyzing the particles emitted from heavy-ion collisions provides information about the QGP and the dynamics of the collision.

### Charged Particle Production in Central Collisions
In central collisions, the number of produced particles is significantly higher than in peripheral collisions.

- **Total Yield**: Provides insights into the overall entropy produced. Higher yields suggest the creation of a large volume of hot, dense matter.
- **Transverse Momentum Spectra**: Reflect both the thermal motion of particles in the hot medium and the collective expansion of the system.

By studying these aspects, we can determine the temperature and collective flow velocity of the system at freeze-out — the point when particles cease to interact strongly.

### Energy Loss Mechanisms in the Quark-Gluon Plasma
Understanding how partons (quarks and gluons) lose energy as they traverse the QGP is needed for characterizing the medium created in heavy-ion collisions.

- **Parton Energy Loss**: As partons move through the hot, dense medium, they interact with other quarks and gluons, losing energy through processes like gluon radiation and collisional energy loss.
- **Transverse Momentum Dependence**: The yield of produced particles at different transverse momenta ($p_T$) offers insights into these energy loss mechanisms. High-$p_T$ particles are especially sensitive to the medium's properties.
- **Centrality Dependence**: In central collisions, where the medium is denser, a higher degree of energy loss is observed. Particles in central collisions traverse a larger volume of the dense medium, losing more energy compared to those in peripheral collisions.

### Nuclear Modification Factor $R_\mathrm{cp}$
The nuclear modification factor $R_\mathrm{cp}$ is an observable used for studying medium effects in heavy-ion collisions. It's defined as the ratio of particle yields in central collisions to those in peripheral collisions, normalized by the number of binary nucleon-nucleon collisions:

$$
R_\mathrm{cp}=
\frac{\left(\frac{1}{N_{{evt, central}}}\frac{dN_{{central}}}{dp_\mathrm{T}}\right)}{\left(\frac{1}{N_{{evt, peripheral}}}\frac{dN_{{peripheral}}}{dp_{T}} \right)} \times \frac{\langle T_{{AA, peripheral}} \rangle}{\langle T_{{AA, central}} \rangle}
$$

- $R_\mathrm{cp}=1$: Indicates no significant medium effect; particle production scales with the number of binary collisions.
- $R_\mathrm{cp}<1$: Suggests suppression of particle yields in central collisions due to energy loss in the medium, consistent with the presence of the QGP.

import Rcpresults from '../images/Rcp_results.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={Rcpresults} alt="Nuclear modification factor vs transversum momentum, associated to collision centrality." style={{width: 650, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 4: Nuclear modification factor vs transversum momentum, associated to collision centrality.</figcaption>
</figure>


Some observations: 
- All measured $R_\mathrm{cp}$ values are typically less than 1, indicating significant medium interactions.
- The more central the collision, the stronger the suppression, reflecting the increased energy loss in a denser medium.

Analyzing $R_\mathrm{cp}$ helps us gain deeper insight into the medium produced during the collision and the mechanisms of particle production and suppression.

### Estimating Initial Energy Density
Analyzing particle spectra allows for the estimation of the initial energy density ($\epsilon$) of the collision. This is done by using measured particle multiplicities and spectra, and applying theoretical models such as hydrodynamic simulations.

Understanding the energy density is important for validating theoretical models and comparing experimental results with predictions from lattice QCD.

### Tracking Efficiency $\epsilon$
Tracking efficiency ($\epsilon $) is defined as the fraction of true charged particles that are successfully matched to reconstructed tracks:

$$
\epsilon = \frac{N_{ch}^{truth}|_\text{matched to reconstructed}}{N_{ch}^{truth}}
$$

A common filter used for matching is $\Delta R=\sqrt{(\Delta\phi)^2+(\Delta\eta)^2}<0.01$ where $\Delta\phi$ and $\Delta\eta$ are the differences in azimuthal angle and pseudorapidity between the true and reconstructed tracks.

In the dense environment of a heavy-ion collision, overlapping tracks and high particle multiplicities can make accurate tracking difficult, necessitating sophisticated algorithms and detector technologies. High tracking efficiency ensures that the measured particle yields and spectra accurately reflect the true particle production in the collision.

## The LHC Heavy-Ion Programme: Present and Future
The Large Hadron Collider (LHC) hosts a comprehensive heavy-ion program dedicated to probing the quark-gluon plasma. By colliding heavy ions at unprecedented energies, the LHC recreates the extreme conditions necessary to study QCD under deconfined conditions.

- **Focus on High Temperatures**: The LHC explores high-temperature, low-baryon-density conditions similar to those of the early universe.
- **Complementary Facilities**: Other facilities like the Relativistic Heavy Ion Collider (RHIC) explore a broader range of densities, including those relevant to neutron stars.

### Goals of the Heavy-Ion Programme
- **Investigate QGP Properties**: Through observables like particle jets, flow patterns, and particle spectra.
- **Map the QCD Phase Diagram**: By varying collision energies and system sizes to explore different regions of temperature and density.
- **Bridge High-Energy Physics and Cosmology**: Studying the QGP evolution provides insights into the early universe's conditions and matter formation.

These efforts contribute to a deeper understanding of how quarks and gluons interact and transition into the stable hadrons that form the matter around us, addressing fundamental questions about the origin and evolution of the universe.

## Learn more
This text is largely based on the heavy ion lectures for the CERN summer student program of 2024. You can watch each part here: [1](https://cds.cern.ch/record/2905838), [2](https://cds.cern.ch/record/2905965) and [3](https://cds.cern.ch/record/2905703).