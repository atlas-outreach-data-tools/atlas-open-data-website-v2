# ATLAS Open Data Documentation
> A Hub for High Energy Physics Learning and Research

Welcome to the ATLAS Open Data Documentation. Our mission is to democratize access to high-energy physics data, empowering a diverse range of users from high school students, to members of the high-energy physics community, and citizen scientists.

This documentation is dedicated to providing extensive data and tools necessary for understanding and analyzing the complex data of real experimental particle physics experiments. Whether you are a student just beginning to explore the field, an educator seeking resources for teaching, a researcher delving into advanced studies, or simply curious about the world of particle physics, the ATLAS Open Data Documentation is your gateway to a myriad of information and knowledge.

We currently offer two major releases of data for educational purposes:

- **8 TeV dataset**: Collected in 2012, this dataset offers a look at collisions at 8 TeV center-of-mass energy. You can find detailed information in the [8 TeV public note](https://cds.cern.ch/record/2624572/files/ATL-OREACH-PUB-2018-001.pdf).
- **13 TeV dataset (legacy)**: Collected in 2016, this dataset provides data at 13 TeV center-of-mass energy. For more details, refer to the [13 TeV public note](https://cds.cern.ch/record/2707171/files/ANA-OTRC-2019-01-PUB-updated.pdf).

A **new 13 TeV dataset**, with data colected in 2015 and 2016, will be available in 2024, expanding the range of educational opportunities. A public note for this release will be published soon.

For research purposes, we also provide more complex datasets:

- **Research release**: This dataset includes detailed data for analysis and advanced studies in particle physics. While we do not yet have a public note for the research release, you can explore all the [available resources](/docs/userpath/researchers) and [datasets](/docs/data/for_research/details) on this website.

Explore ATLAS Open Data to embark on a journey of discovery and innovation in the fascinating world of high-energy physics.

