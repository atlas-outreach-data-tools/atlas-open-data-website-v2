# Reconstruction
Electrons and photons are reconstructed from the characteristic information they leave while passing through the detector. Once released from the interaction point in a collision, these particles travel first through the inner detector (ID), which consists of multiple layers of silicon detectors and a straw tracker layer, to get to the calorimeter.

- **Electrons**: As electrons pass through the inner detector, they are bent by the magnetic field and interact with the material, creating electrical signals known as "hits". These hits are used to form tracks that trace the electron’s path through the detector. After passing through the inner detector, the electron enters the calorimeter. When hitting the electromagnetic calorimeter (EMCal), the electron generates an "electromagnetic shower" composed of secondary electrons and photons. This shower is created through processes like bremsstrahlung (radiation of photons due to deceleration) and pair production (conversion of photons into electron-positron pairs). These interactions continue until the electron's energy is fully absorbed by the calorimeter material.

- **Photons**: Photons, unlike electrons, do not produce tracks in the inner detector because they are electrically neutral. However, when they enter the electromagnetic calorimeter, they also generate electromagnetic showers. In the electromagnetic calorimeter, photons primarily interact through pair production and the Compton effect, resulting in the creation of electron-positron pairs and scattered photons. As these secondary electrons and positrons travel through the calorimeter, they further interact with the material, creating more electrons and photons, thus propagating the shower. The energy of the original photon is eventually fully absorbed by the calorimeter material, similar to the process for electrons.

In the following part we will briefly explain how the signal left in the detector is transformed into electrons and photons for analysis. This text is based on [Electron reconstruction and identification in the ATLAS experiment using the 2015 and 2016 LHC proton–proton collision data at √s = 13 TeV](https://arxiv.org/pdf/1902.04655).

## Seed-Cluster Reconstruction
The first step in reconstructing electrons and photons is identifying the initial clusters of energy deposits in the electromagnetic calorimeter, a process known as seed-cluster reconstruction. 

Seed cluster reconstruction begins by identifying cells in the calorimeter with significant energy deposits, the significance is given by:

$$
\zeta = \left|\frac{E^{EM}_{cell}}{\sigma^{EM}_{cell}}\right|,
$$

where $E^{EM}_{cell}$ is the energy in the electromagnetic cell and $\sigma^{EM}_{cell}$ is the expected noise in that cell.

For Run 2, the reconstruction process involved using a sliding-window algorithm to scan the calorimeter data, looking for regions where the energy deposition is significantly higher than the surrounding cells, typically several times above the expected noise level. Each of these regions, or "towers," consisted of a small cluster of adjacent grid elements, defined in eta ($\eta$) and phi ($\phi$) coordinates. When the algorithm detected a localized energy deposit, it formed a seed cluster by summing the energy from a window of grid elements surrounding the peak deposit, typically spanning a 3x5 grid in $\eta$ and $\phi$. If two seed clusters overlapped, the algorithm retained the one with the higher energy, provided its energy was significantly greater than the other. This ensured that only the most relevant clusters were selected for further analysis.

The current approach uses a more dynamic method. The process still begins by scanning the calorimeter for seed cells. Once a seed cell is identified, the algorithm grows the cluster by examining neighboring cells. Each neighboring cell is evaluated based on its energy deposit and spatial proximity to the seed cell. Cells with energy deposits above a lower threshold are added to the cluster, ensuring that the cluster grows in a way that captures the full extent of the energy deposition. This iterative process continues, expanding the cluster until no more neighboring cells with significant energy deposits are found.

The seed cluster reconstruction often involves identifying primary clusters (seed clusters) and secondary clusters. Secondary clusters, or satellite clusters, arise from nearby energy deposits caused by processes like bremsstrahlung radiation (for electrons) or photon conversions into electron-positron pairs. These secondary clusters are then associated with the primary seed cluster to form a supercluster, which represents the total energy deposition of the particle and any secondary interactions. The formation of superclusters will be discussed later.

## Track Reconstruction and Cluster Matching for Electrons
Track reconstruction for electrons begins with the collection of "hits" in the inner detector, which consists of multiple layers, including silicon pixel detectors, silicon strip detectors, and a Transition Radiation Tracker (TRT). As charged particles such as electrons traverse these layers, they interact with the detector material, creating electrical signals at specific points along their path. These signals, known as hits, are the raw data used to trace the trajectory of the particles.

The hits are first collected and grouped into clusters. In the pixel detector, each hit forms a space-point, while in the silicon strip detector, the signals detected from multiple angled layers (stereo views) are used together to determine the exact three-dimensional position of a particle hit. An overview of the track reconstruction and cluster matching is explained below:

**1. Hit Collection and Track Seeding**: Using these space-points, initial track seeds are formed. These seeds are typically triplets of space-points that suggest a possible trajectory of a particle. This phase involves pattern recognition, where the algorithm searches for sequences of hits that are likely to belong to the same particle. The track seeds provide initial guesses for the particle trajectories.

**2. Pattern Recognition and Track Fitting**: Extending these track seeds into full tracks requires combinatorial techniques to evaluate all possible paths the particle might have taken through the detector. The algorithm fits these paths to the hits, iteratively adjusting the track parameters to find the best match. During this step, the algorithm must also account for the effects of the magnetic field and the energy loss through bremsstrahlung. This process can cause electrons to lose energy and change direction, leading to multiple tracks being associated with a single electron, especially in areas where bremsstrahlung is significant.

**3. Bremsstrahlung Consideration and Advanced Fitting**: Special attention is given to energy loss through bremsstrahlung, allowing for multiple tracks to be associated with the same electron if needed. To refine the tracks further, we use a method known as the Gaussian-Sum Filter (GSF). The GSF method models the non-linear effects of energy loss, providing a more accurate description of the electron's trajectory. This fitting algorithm minimizes the overall deviation by considering the energy lost due to bremsstrahlung, resulting in a more precise track reconstruction.

**4. Ambiguity Resolution**: During the fitting process, it is possible for multiple tracks to share hits or overlap. Ambiguity resolution algorithms help determine the best possible track candidates, resolving overlaps. The result of this process is a set of detailed tracks representing the paths of charged particles through the inner detector.

**5. Track-Cluster Matching:** The final step involves matching these reconstructed tracks to the energy clusters identified in the electromagnetic calorimeter. This matching process involves comparing the track’s predicted impact points in the calorimeter with the positions of the clusters. The algorithm checks if the trajectory of a track intersects with a cluster in both $\eta$ and $\phi$ coordinates. If multiple tracks are found to match a single cluster, additional criteria are used to refine the matching, such as the quality of the track fit and the consistency of the track’s energy with the cluster's energy deposit. By integrating the trajectory data from the inner detector with the energy measurements from the electromagnetic calorimeter, the algorithm ensures that the recorded energy of the cluster reflects the electron's total energy.

## Conversion Vertex Reconstruction and Cluster Matching for Photons
For photons, which do not produce tracks in the inner detector because they are electrically neutral, a different approach is used for their reconstruction. Photons can convert into electron-positron pairs within the detector, and these conversions are important for reconstructing the photon’s path and energy. The process is as follows:

**1. Vertex Identification:** When a photon converts into an electron-positron pair, the resulting charged particles leave hits in the inner tracker. The point where this conversion occurs is called the conversion vertex. This vertex is identified by finding pairs of tracks that originate from a common point. The algorithm searches for these pairs of tracks, ensuring they intersect at a single location, which indicates the conversion point of the photon.

**2. Track Fitting:** The tracks of the electron and positron are fitted using techniques similar to those used for single electrons. However, additional constraints are applied to ensure that the tracks intersect precisely at the conversion vertex. This fitting process accounts for the trajectories of both particles and their interactions with the detector material, providing a detailed reconstruction of the conversion event.

**3. Vertex-Cluster Matching:** Once the conversion vertices are identified, they need to be matched to the clusters in the electromagnetic calorimeter. The initial step involves matching the conversion vertex to clusters based on spatial correlation. The algorithm compares the position of the vertex with the location of the energy clusters in the electromagnetic calorimeter, looking for a close match. Similar to track-cluster matching for electrons, further criteria are used to refine the vertex-cluster matching. This refinement process includes ensuring that the energy associated with the conversion vertex matches the energy of the cluster in the electromagnetic calorimeter. The algorithm evaluates the consistency of the vertex’s energy with the cluster’s energy deposit, adjusting the matches to achieve the best correlation.

## Supercluster Formation
After matching the clusters to the tracks or vertices, superclusters are formed. The supercluster formation utilizes the clusters obtained from the seed-cluster reconstruction, with independent processes for electron and photon reconstruction.

The seed topo clusters are tested for use as seed clusters:

- Electron seeds require $E_T > 1$ GeV and at least 4 silicon hits.
- Photon seeds require $E_T > 1.5$ GeV.

The topo clusters near the seed clusters are defined as **satellite clusters**. The ensemble of the seed topo cluster and its satellites is known as a supercluster.

Satellite clusters are added to seed clusters to form superclusters based on specific criteria:

- Add all clusters within a $3 \times 5$ grid in $\eta$ and $\phi$ around a seed cluster for both electrons and photons.
- For electrons, add a satellite cluster if it matches the same track as the seed cluster.
- For photons, add a satellite cluster if it matches the same conversion vertex as the seed cluster or if it has a track that is part of the conversion vertex matched to the seed cluster.

## Resolving Ambiguities Between Electrons and Photons
The reconstructuon of electrons and photons happens independently and in parallel. This means that, it can result in the same seed cluster being matched both to an electron track and to a photon conversion vertex. This overlap leads to an ambiguity. The ambiguity arises because electrons and photons, despite being different particles, can produce similar energy deposition patterns in the electromagnetic calorimeter. Electrons, when they pass through the detector, create electromagnetic showers that include photons due to bremsstrahlung radiation. Conversely, photons can convert into electron-positron pairs, generating tracks in the inner detector. Therefore, both particle types can be associated with clusters of energy deposits and tracks, leading to potential overlaps in their reconstruction.

To resolve this ambiguity, an ambiguity resolution step is employed. Initially, track reconstruction for electrons and vertex reconstruction for photons are conducted separately. The ambiguity resolution step then determines whether an object can be uniquely identified as either a photon or an electron. If the identification is clear and unambiguous, only one particle (either the electron or the photon) is stored. However, if the identification is not clear and both classifications are possible, both objects (an electron and a photon) are created and stored.

To decide whether a cluster should be classified as an electron, a photon, or both, several criteria are considered. The spatial correlation between the track (for electrons) and the cluster in the electromagnetic calorimeter, and the energy consistency between the track and the cluster, are evaluated. For electrons, the presence of a bremsstrahlung radiation pattern can help distinguish them from photons. For photons, the identification of a conversion vertex where the photon converts into an electron-positron pair can be a distinguishing factor.

The process of resolving ambiguities between electrons and photons significantly affects the data quality and subsequent analysis. By applying strict criteria to resolve ambiguities, the rate of "fake" electrons or photons is reduced. However, there is always a tradeoff between efficiency and purity in particle identification. The criteria used for resolving ambiguities can vary in strictness, with tighter criteria increasing the purity of the identified particles but potentially reducing the efficiency of detecting real particles.

To address this, different "working points" are defined, ranging from 'Very Loose' to 'Tight.' These working points allow to choose the appropriate balance. As the working points get tighter, the rate of "fake" electrons decreases, but so does the efficiency of selecting real electrons. This tradeoff is an important consideration in any analysis.
