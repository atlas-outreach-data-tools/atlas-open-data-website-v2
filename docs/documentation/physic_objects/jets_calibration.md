# Hadronic Calibrations
Hadronic calibrations are part of the data analysis framework of the ATLAS experiment. These calibrations are important for accurately determining the properties of hadrons and the jets produced by the collisions. The ATLAS detector is designed to capture details of the particle interactions, however, moving from detector data to physical measurements requires an understanding of the detector's response to particles. This section aims to briefly explain the practices of hadronic calibrations and jet reconstruction within the ATLAS experiment, highlighting their role in ensuring the accuracy of data interpretation.

## Why Hadronic Calibrations?
In every physics experiment we want to lower uncertainties as much as possible. As an experiment gets more complex, one has to account for the effects of anything that may introduce uncertainty in our measurements. Calibrations are necessary to account for different ways in which the measurements taken by the ATLAS experiment may differ from the physical process that we try to measure. The following is a non-comprhensive list of some of the reasons why we do hadronic calibrations at ATLAS:
- **Non-compensating calorimeter response**: All calorimeters employed in ATLAS are non-compensating, meaning their signal for hadrons is smaller than the one for electrons and photons depositing the same energy (e/$\pi$ > 1). Applying corrections to the signal locally so that e/π approaches unity on average improves the linearity of the response as well as the resolution for jets built from a mix of electromagnetic and hadronic signals. It also improves the reconstruction of full event observables such as $E_T^{miss}$, which combines signals from the whole calorimeter system and requires balanced electromagnetic and hadronic responses in and outside signals from (hard) particles and jets.
- **Sampling calorimeters**: ATLAS calorimeters are sampling calorimeters, meaning that the active material where we measure the energy is surrounded by inactive material. A correction is needed to address the limitations in the signal acceptance in active calorimeter regions due to energy losses in nearby inactive material in front, between, and inside the calorimeter modules.
- **Detector construction**: The detector's construction is not perfect. It has what we call "dead spaces", which are parts in which no measurement can be taken due to construction, e.g. in the cryostat, the magnetic coil and calorimeter intermodular cracks. We have to correct for these dead spaces to account for the energy that wasn't measured.

## Jet Energy Scale Calibration 
The jet energy scale calibration restores the jet energy to that of jets reconstructed at the particle level. The full chain of corrections is illustrated in Figure 2. All stages correct the four-momentum, scaling the jet transverse momentum, energy, and mass.

import hadronicalstep from '../images/hadronic_cal_steps.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={hadronicalstep} alt="Stages of jet energy scale calibrations. Each one is applied to the four-momentum of the jet." style={{width: 800, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 3: Stages of jet energy scale calibrations. Each one is applied to the four-momentum of the jet.</figcaption>
</figure>

The following explanations are based on [New techniques for jet calibration with the ATLAS detector](https://arxiv.org/pdf/2303.17312) and [Jet energy scale and resolution measured in proton–proton collisions at $\sqrt{s}$ = 13 TeV with the ATLAS detector](https://arxiv.org/pdf/2007.02645). For further details on the specific of the methods, refer to the papers.

### Pile-up Substraction
As luminosity in the LHC increases, the number of interaction during a beam crossing increases. These extra interactions are collectively referred to as “pile-up”, and its presence can have an impact on physics analyses. 

Pile-up subtraction aims to remove the excess energy due to additional proton–proton interactions within the same (in-time) or nearby (out-of-time) bunch crossings. Two types of corrections are applied: one to handle the average effect of pile-up and another to fine-tune for residual effects that are not captured by the first correction. 

#### Jet pT-Density-Based Subtraction
The extent to which a jet might be influenced by pile-up is determined by looking at two things: the 'area' (A) of the jet and the pile-up density ($\rho$) in the event. 

- The 'area' of a jet is found by counting how many simulated, nearly momentum-less particles, known as 'ghost particles', are enclosed within the jet's boundaries when the particles are grouped.

- The event's pile-up density is gauged by taking the median transverse momentum per unit area, $\langle p_t/A\rangle$, from a group of pile-up 'jets' in the central region of the detector. These are not real jets but represent the organized noise from many minor collisions. This grouping is done using the anti-$k_t$ algorithm, which is good at clustering this background noise.

After determining these two factors, we correct each jet's transverse momentum to remove the pile-up effect, using the expression:
$$ 
p^{corr}_{t} = p^{reco}_{t} - A\rho, 
$$

where $p^{reco}_{t}$ refers to the transverse momentum of the reconstructed jet before any pileup correction. The method assumes that the pileup is uniform across the detector and that the number of pileup jets is much larger than the number of hard scatter jets with transverse momentum above certain threshold, ensuring the median is not biased by actual collision events.

This process involves additional steps and considerations not fully detailed here. For a more comprehensive explanation, refer to the paper [Pileup subtraction using jet areas](https://arxiv.org/pdf/0707.1378.pdf).

#### Residual Pile-Up Corrections
The $\rho$ calculation is derived from the central regions of the calorimeter and does not fully describe the pile-up sensitivity in the forward calorimeter region. Hence, some pile-up effects remain and further corrections are needed. 

The additional pile-up effect that still remains after the first correction is studied by looking at how the measured jet momentum deviates from the truth jet momentum (which we would expect in a perfect, pile-up-free world). This deviation is studied as a function of variables like $N_{PV}$ (number of primary vertices, which indicate the number of collisions) and $\mu$ (mean number of interactions per bunch crossing). These tell us about the amount of pile-up. 

The final corrected jet momentum is calculated by taking the initially measured momentum and subtracting both the average pile-up and any remaining extra effects:
$$ 
p^{corr}_{t} = p^{reco}_{t} - \rho A - \alpha(N_{VP}-1) - \beta\mu
$$

$\alpha$ and $\beta$ are determined within various groupings based on the true transverse momentum of the jet that corresponds to an ideal measurement without pile-up, and the detector pseudorapidity ($\eta$), which indicates the jet's position relative to the center of the detector.

### Simulation Based Calibrations

#### Jet energy scale and $\eta$ calibration
The jet energy scale (JES) and directional calibrations correct the mesuarements of a jet's energy and angle to reflect the true energy and direction of the particles before they interact with the detector. 

The jets measured by the detector (known as reconstructed jets) are aligned with the truth jet within a certain distance. It's required that there be no other significant jets close by to avoid confusion in the matching process. For each pair of reconstructed and truth jets the response is calculated as

$$ 
R(E^{truth}_{jet}, \eta_{det}) = E^{reco}_{jet}/E^{truth}_{jet}
$$

in bins of the truth jet energy and the pseudorapidity, where the pseudorapidity ($\eta_{det}$) is pointing from the geometric center of the detector, used to remove any ambiguity about which region of the detector is measuring the jet. The average response is parameterized as a function of $E^{reco}$ using a numerical inversion procedure.

If we plot the energy response versus $\eta_{det}$ before and after calibration (Fig. 4) we can see how the response approaches 1, i.e., how the energy of the reconstructed jets is closer to that of the true jet, which is what we aim to achieve with the calibration.

import jetresponse from '../images/jet_calresolution.jpg';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={jetresponse} alt="Jet response at fixed energies as a function of $\eta$ (a) before calibration (b) after calibration." style={{width: 800, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 4: Jet response at fixed energies as a function of $\eta$ (a) before calibration (b) after calibration.</figcaption>
</figure>

There are small biases on the direction of jets after the energy correction, and an additive correction with a similar approach to the JES calibration is applied to correct them.

#### The global property calibration
The jet energy scale and $\eta$ calibration adjust the jet energy response based on energy and pseudorapidity. However, as explained in [this reference](https://arxiv.org/pdf/2303.17312), other factors also influence the energy response, including:

- The distribution of energy within the jet.
- The distribution of energy deposits across different calorimeter layers.
- The types of hadrons produced in the jet.

These characteristics vary depending on whether the jet originates from a quark or a gluon. Quark-initiated jets generally have fewer hadrons with a higher fraction of the jet's $p_T$, leading to deeper calorimeter contributions. In contrast, gluon-initiated jets typically contain more hadrons with lower $p_T$, resulting in a lower calorimeter response and a broader transverse profile.

The jet $p_T$ response is also affected by the Monte Carlo (MC) model. While most MC predictions show similar behavior for quark-initiated jets, the differences for gluon-initiated jets can be significant.

To reduce jet-to-jet response variations, the global property calibration is employed, using two methods: global sequential calibration (GSC) and global neural network calibration (GNNC).

**Global Sequential Calibration (GSC)** applies a series of multiplicative corrections to account for differences in calorimeter responses to different types of jets, improving jet resolution without altering the jet energy response. It relies on global jet observables such as:
- The longitudinal profile of energy deposits in the calorimeters.
- Tracking information matched to the jet.
- Activity in the muon chambers behind the jet.

Each correction to the jet four-momentum is derived and applied independently and sequentially. However, GSC is constrained to using relatively uncorrelated variables for correction. Using correlated variables would cause each sequential step to interfere with previous corrections.

The **Global Neural Network Calibration (GNNC)** addresses the limitations of GSC. A deep neural network (DNN) is trained to perform a simultaneous correction using a wide variety of jet properties. This allows the use of correlated variables for determining the global jet property correction. Unlike GSC, the DNN is designed to correct the jet $p_T$ response, which is crucial for analyses based on jet $p_T$.

Both calibration methods have a small overall impact but significantly improve the calibrations for different classes of jets, enhancing energy resolution. Additionally, these calibrations reduce differences between MC predictions for the jet energy scale, leading to smaller modeling uncertainties.

### In-situ Analysis
All the steps previously explained aim to correct jets on the particle level to get simulated and data jets closer to having the truth energy and mass. Now, we want to account for the differences between data an simulation. This differences are caused by imperfections in simulation of the detector material or the involved physics processes. **In this calibration we calibrate data to match the simulation**, given that the MC based calibrations calibrated the simulation to match the true energy scale, which is the scale in which we would like to have the data.

import insitu from '../images/insitu_cal.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={insitu} alt="The in-situ jet energy scale calibration process." style={{width: 700, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 5: The in-situ jet energy scale calibration process.</figcaption>
</figure>

The in situ calibration provides validation of the previos MC calibration of jets by comparing the data-to-MC difference between the $p_T$ balance of a jet against a well-calibrated object or system. For this, the jet response is as the average ratio of the jet $p_T$ to the reference object $p_T$:

$$
R_{\text{\it{in situ}}} = \Biggl\langle\frac{p_T^{jet}}{p_T^{ref}}\Biggr\rangle ,
$$

where the reference objects are $Z(\rightarrow\mu\mu)$, $Z(\rightarrow ee)$ or $\gamma$. However, $R_{\text{\it{in situ}}}$ is sensitive to effects such as the presence of additional radiative jets or the transition of energy into or out of the jet cone. A double ratio, insensitive to these secondary effects provided they are well-modelled in simulations, is defined:
$$
C = \frac{R_{\text{\it{in situ}}}^{data}}{R_{\text{\it{in situ}}}^{MC}}
$$

The calibration factor to the jet four-momentum can be obtained by a numerical inversion of this doubleratio as a function of jet $p_T$ , and as a function of $\eta_{\text{det}}$ in $\eta$-intercalibration.

In in-situ analysis, there are two sequential steps: relative in-situ calibration (or $\eta$-intercalibration) and absolute calibration.

The **relative in-situ calibration** aligns the energy scale of forward jets ($0.8 < |\eta_{det}| < 4.5$) with the energy scale of central jets ($|\eta_{det}| < 0.8$). This alignment is achieved using the transverse momentum ($p_T$) balance in a dijet system, where forward jets are generally better understood. The calibration involves calibrating all regions relative to each other by solving a set of linear equations, a process known as the **matrix method**.

The **absolute calibration** adjusts the absolute jet energy scale in data to match the scale in simulation. This is done by exploiting the $p_T$ balance between the hadronic recoil and a well-calibrated reference object, such as a $Z$ boson or a photon. The following methods are aimed to achieve this:

1. **Direct Balance (DB)**: This method balances the $p_T$ directly between the reference object and the jet.
2. **Missing Projection Fraction (MPF)**: This method balances the $p_T$ between the reference object and the entire hadronic recoil of the detector. The MPF method is the most commonly used.
3. **Multi-Jet Balance (MJB)**: For higher $p_T$ jets, this method uses already calibrated lower-$p_T$ jets as reference objects to balance with the higher $p_T$ jets.

The results from different absolute in-situ calibrations are then statistically combined to achieve a final calibration.

## More Information
For further details on the specific of the introduced methods, refer to the papers [New techniques for jet calibration with the ATLAS detector](https://arxiv.org/pdf/2303.17312) and [Jet energy scale and resolution measured in proton–proton collisions at $\sqrt{s}$ = 13 TeV with the ATLAS detector](https://arxiv.org/pdf/2007.02645).
