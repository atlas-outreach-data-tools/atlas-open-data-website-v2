# Jets
In many high-energy physics experiments, particularly those involving proton collisions, quarks and gluons (collectively known as partons) play a significant role. Accurately reconstructing these partons is essential, yet challenging due to several factors:

- **Parton Isolation and Color Charge:**
    Partons carry a property called color charge and cannot be observed in isolation. As they move apart, it becomes energetically favorable to create new quarks from the vacuum. These quarks quickly combine to form a spray of stable particles called hadrons, which then enter the detector, leaving tracks and significant energy deposits.
- **Color Connectivity:**
    Hadrons resulting from the same color singlet (a group of partons that start together) may carry energy from multiple partons, complicating the energy measurements.
- **Radiative Corrections:**
    Individual quarks or gluons are not stable, observable objects due to higher-order radiative corrections that affect the calculations of their cross sections. To manage this, soft radiations are combined back with the original parton.

To address these complications, hadronic jets are used. These jets are clusters of energy deposits in the detector, grouped together to represent the original parton. Various algorithms can be applied to reconstruct jets from different types of objects, such as calorimeter deposits, tracks, stable truth-level hadrons, and partons. 

There are a couple of points unique to jets that are worth keeping in mind:
- Jets serve as the physical proxies for partons. Unlike particles like muons, which are directly observed, jets represent a concept that helps us understand the partons' behavior.
- There is no single "correct" jet definition at the truth level. The choice of jet reconstruction algorithm defines the jets, and different definitions may be more suitable for different purposes.

Jets are large objects in detectors that often include contributions from pileup events, which are additional interactions happening simultaneously within the same collision event. To estimate the original "truth" jets formed from stable hadrons, it is essential to correct for these contributions. It is worth noting that collision events produce many colored particles, resulting in numerous jets, and identifying the energy deposits corresponding to the original partons can be complex but is necessary for precise measurements.

In the following sections you'll find information about the jet reconstruction, calibration and tagging done by the ATLAS experiment.
