# Electron and Photons
Two of the best known particles detected with ATLAS are the electrons and photons. Electrons belong to the first generation of leptons of the [Standard Model](../introduction/SM_and_beyond) and are believed to be elementary particles because they have no known components or substructure. Photons, on the other hand, are one of the bosons thta mediate the electromagnetic force.

Electrons are important for multiple analyses: low-energy electrons allow us to study [quarkonia](https://en.wikipedia.org/wiki/Quarkonium), medium energy electrons give access to [electroweak physics](https://arxiv.org/pdf/hep-ph/0501246), and high energy electrons are a promising channel for many types of physics beyond the Standard Model. Photons, ranging in energy from very low energies up to several hundreds GeV, also serve a variety of calibration and signal purposes. For instance, $H\rightarrow\gamma\gamma$ is a central Higgs discovery channel in many scenarios.[ref](https://inspirehep.net/files/2a8b365159828edb635eace68b918f7f).

In the following sections we will explain how electrons and photons are reconstructed, calibrated, differentiated and isolated. 
