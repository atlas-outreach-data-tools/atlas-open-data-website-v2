# Reconstruction
Jets are showers of collimated particles, made up mainly of hadrons, but also photons and leptons, and are the experimental signature of quarks and gluons in high energy proton-proton collisions. Due to their high production rate, jets have become a target of study to "rediscover" processes expected from the standard model, ensure that detectors behave correctly and search for new physics. 

A jet is formed from the measurements of a set of neutral colored particles and its definition is not unique. In fact, the existence of a jet is dependent on the mathematical rule that defines it. These mathematical rules groups the constituents of the jet according to kinematic properties and are known as jet clustering algorithms. 

## Inputs
Four-vector objects are used as inputs to reconstruction algorithms, and can be:
- **Stable particles defined by MC generators**: particles at the MC generator level are referred to as truth particles. Jets reconstructed from these truth particles are known as "truth jets".
- **Charged-particle tracks**: this method uses the trajectories and momentum information of these charged particles to infer the properties of the jet. Track-based jet reconstruction is particularly useful in certain contexts as pile-up mitigation and jet tagging. The result is referred to as "track jet".
- **Calorimeter energy deposits**: calorimeter cells are first clustered into topological clusters (topo-clusters) using a nearest-neighbour algorithm. Calibrations are then applied to the clusters, some are: the hadron-like clusters are subject of a cell weighting procedure to compensate for the lower responce of the calorimeter to hadronic deposits, out-of-cluster (OOC) corrections for lost energy deposited in calorimeter cells outside of reconstructed clusters in the tail of hadronic shower, dead material (DM) corrections are applied on the cluster level to account for energy deposits outside of active calorimeter volumes. Jets reconstructed using only calorimeter-based energy information use the corrected EM scale topo-clusters and are referred to as EMtopo jets. 
- **Algorithmic combinations of tracks and energy deposits**: Hadronic final-state measurements can be improved by making more complete use of the information from both the tracking and calorimeter systems. As explained in [[ref1]](https://cds.cern.ch/record/2824558/files/ATL-PHYS-PUB-2022-038.pdf):
    - Tracks can only be reconstructed from charged-particles hits in the inner detector, while topo-clusters are built from interactions in the calorimeters of both charged and neutral particles.
    - The angular resolution of the inner detector is much better than that of the calorimeters. Therefore tracks can be associated to the different vertices, while this is not possible for topo-clusters.
    - For low-energy charged particles, the momentum resolution of the inner detector is significantly better than the energy resolution of the calorimeters. On the contrary, at high energy, the energy resolution of the calorimeters is better than the momentum resolution of the inner detector.
    - The inner detector covers only the region |𝜂| < 2.5, while the calorimeters cover the region |𝜂| < 4.9.

    Some algorithms that use topo-clusters and tracks are [Track-CaloClusters (TCC)](https://cds.cern.ch/record/2275636/files/ATL-PHYS-PUB-2017-015.pdf), [Particle Flow (PFlow)](https://arxiv.org/pdf/1703.10485.pdf), and [Unified Flow Objects (UFO)](https://cds.cern.ch/record/2824558/files/ATL-PHYS-PUB-2022-038.pdf). 

import typejets from '../images/type_jet.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={typejets} alt="Different stages of jet development" style={{width: 650, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 1: Different stages of jet development.</figcaption>
</figure>

## Algorithms
As stated above, the definition of a jet depends on the mathematical rule that defines it. A clustering algorithm is expected to possess certain characteristics, as discussed in [[ref2]](https://iopscience.iop.org/book/mono/978-0-7503-2112-9/chapter/bk978-0-7503-2112-9ch3):

- The physics interpretation of the event should not depend too sensitively on the jet definition.
- They should be independent of the design/structure of the detector.
- Minimal sensitivity to hadronisation, underlying events, and pile-up is desired, but is not always possible in practice.
- Experimentally, they should be easy to implement and (computationally) quickly executable.
- Theoretically, they need to be infrared and collinear safe to not have divergent cross-sections in perturbative calculations. Infrared safety means that the addition of a soft gluon should not change the results of the jet clustering. Collinear safety implies that splitting one parton into two collinear partons should not change the results of the jet clustering.

There are two main types of clustering algorithms: cone algorithms and sequential recombination algorithms.
- **Cone algorithms** assume that the jet lies in conic regions in $(\theta-\phi)$ space, so the jets reconstructed by these algorithms have circular edges. They are easy to implement, but are not collinearly stable. Examples of cone algorithms are: Midpoint Cone, used in Tevatron, Iterative Cone and SISCone.
- **Sequential recombination algorithms** assume that the components of a jet possess a small transverse momentum difference. Thus, particles are clustered in momentum space, resulting in jets with fluctuations in $(\theta-\phi)$ space. 

The most commonly used recombination algorithms are [$k_t$](https://doi.org/10.1103/PhysRevD.48.3160), [anti-$k_t$](https://iopscience.iop.org/article/10.1088/1126-6708/2008/04/063) and [Cambridge/Aachen](https://iopscience.iop.org/article/10.1088/1126-6708/1997/08/001), with anti-$k_t$ being the most widely used. It requires an external parameter, the jet radius *R*, which specifies up to what angle the separate partons recombine into a single jet.

import typeclusters from '../images/type_clusters.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={typeclusters} alt="The four main jet reconstruction algorithms’ areas, performed on the same data with the same input radius" style={{width: 750, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 2: The four main jet reconstruction algorithms’ areas, performed on the same data with the same input radius.</figcaption>
</figure>
