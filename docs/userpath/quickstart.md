# 🚀 Quick start
> *The quickest way to start learning with ATLAS Open Data.*

Welcome to the ATLAS open data! You are currently in the *quick start* path, designed to help you start using the data as quickly as possible. In this path, we will focus on using **notebooks**.

### Getting Started with Notebooks
We offer a wide selection of analyses in the form of notebooks in the [13 TeV Open Data Tutorials for Education](../category/13-tev-tutorials-for-education). Within the [analysis notebooks](../category/analysis-notebooks) subsection, you can find various analyses from the Standard Model and beyond. We recommend starting with the [Higgs to γγ analysis](../13TeVDoc/13tutorial#higgs-to-γγ-analysis-new). 

If you're unsure how to run the notebook, check out our guide on [choosing your environment](../category/choose-your-enviroment), particularly [this subsection on using Binder](../13TeVDoc/enviroments/online#binder). This analysis is explained in detail in the [example analysis section](../documentation/example_analyses/analysis_examples_education_2020#example-of-physics-analysis).

For those interested in using C++, explore the [Introductional Notebook to HEP Analysis in C++](../13TeVDoc/13tutorial#an-introductional-notebook-to-hep-analysis-in-c).

### General Information
For general information about the LHC and the ATLAS experiment, we encourage you to check out the following sections:
- [Overview of the Large Hadron Collider](../documentation/introduction/introduction_LCH)
- [Introduction to the ATLAS Experiment](../documentation/introduction/introduction_ATLAS)
- [The Standard Model of Particle Physics and Beyond](../documentation/introduction/SM_and_beyond)
- [ATLAS Data Collection](../documentation/data_collection)
- [Citing ATLAS](../documentation/ethical_legal/citation_policy)
- [The ATLAS Glossary](../documentation/appendix/glossary)

### Data for Education
If you are curious about open data for education and want to know how to access data beyond the proposed analyses, check out these sections:
- [Data for Education](../documentation/introduction/purpose_data_education)
- [8 TeV Data for Education](/docs/data/for_education/8TeV_details)
- [13 TeV Data for Education](/docs/data/for_education/13TeV_details)
- [N-tuple](../documentation/data_format/ntuple)
- [Accessing the Data](/docs/data/index.md)

### Additional Tutorials
For more quick hands-on tutorials, explore the [list of notebooks](../category/analysis-notebooks) available to run without needing to set up an environment.
