# Path for Teachers
*Insert description of path for teachers*
## General information
- [Overview of the Large Hadron Collider](/docs/documentation/introduction/introduction_LCH)
- [Introduction to the ATLAS experiment](/docs/documentation/introduction/introduction_ATLAS)
- [The Standard Model of Particle Physics and Beyond](/docs/documentation/introduction/SM_and_beyond)
- [ATLAS Data Collection](/docs/documentation/data_collection)

## Information for Analyses
- [Data for Education](/docs/documentation/introduction/purpose_data_education)
- [8 TeV Data for Education](/docs/data/for_education/8TeV_details)
- [13 TeV Data for Education](/docs/data/for_education/13TeV_details)
- [NTuple](/docs/documentation/data_format/ntuple)
- [Accessing the Data](/docs/data/index.md)
- [Example Analyses with the 13 TeV Data for Education](/docs/documentation/example_analyses/analysis_examples_education_2020)
