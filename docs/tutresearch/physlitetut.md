# Using the PHYSLITE Format
The research data is available in the [PHYSLITE format](../documentation/data_format/physlite.md), which is user-friendly and ready for analysis. This notebook demonstrates how to utilize ATLAS Open Data in PHYSLITE format using `uproot` and `awkward` arrays for a basic physics analysis. Specifically, it shows how to reconstruct the hadronically decaying top quark from semi-leptonic $t\bar{t}$ events.

:::notebook [PHYSLITE Tutorial](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/for-research/physlite_tutorial.ipynb)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=for-research%2Fphyslite_tutorial.ipynb)
:::

## What's Inside the Notebook
In this notebook, you will learn:
- How to read PHYSLITE data with `uproot` and inspect its branches.
- How to compile branches into records.
- How to perform basic event and object selection.
- How to conduct basic overlap removal.

These steps will guide you to the top quark reconstruction.

## Transforming PHYSLITE to NTuple
To convert PHYSLITE to an NTuple, follow these sections from the [Analysis Software Tutorial](https://atlassoftwaredocs.web.cern.ch/analysis-software/ASWTutorial/):

1. **Basic Analysis Algorithm**: From [Introduction to Algorithms](https://atlassoftwaredocs.web.cern.ch/analysis-software/AnalysisSWTutorial/alg_basic_intro/) to [Run Algorithm](https://atlassoftwaredocs.web.cern.ch/analysis-software/AnalysisSWTutorial/alg_basic_run/).
2. **CP Algorithms**: All subsections starting with [Common CP Algorithms Introduction](https://atlassoftwaredocs.web.cern.ch/analysis-software/AnalysisSWTutorial/cpalg_intro/).
3. **Physics Objects**: [Electrons in Analysis](https://atlassoftwaredocs.web.cern.ch/analysis-software/AnalysisSWTutorial/obj_el_analysis/), [Muons in Analysis](https://atlassoftwaredocs.web.cern.ch/analysis-software/AnalysisSWTutorial/obj_mu_analysis/), [Jets in Analysis](https://atlassoftwaredocs.web.cern.ch/analysis-software/AnalysisSWTutorial/obj_jet_analysis/).

For these you will need to use AnalysisBase. Check how to setup a container in the [next section](containers).

## Resources

Downloading all the available Open Data requires significant resources. For those who wish to tinker, but might not have the computing resources to hand, there are a few options.

- There is a [cluster at the University of Nebraska-Lincoln](https://coffea-opendata.casa/) on which an account can be requested. It's possible to authenticate with Google, for example (no institutional affiliation is required).
- Google offers [cloud resources](https://cloud.google.com/), with free credit for new users.

On these resources, we recommend installing dependencies via a terminal:

<verbatim>
pip3 install --user jupyterlab matplotlib tqdm xrootd zstandard uproot==5.1.2 awkward==2.5.0 vector==1.1.1 cernopendata-client[xrootd]
</verbatim>

If you wish to run without `xrootd`, this is sufficient:

<verbatim>
pip3 install --user jupyterlab matplotlib tqdm zstandard uproot==5.1.2 awkward==2.5.0 vector==1.1.1 cernopendata-client
</verbatim>

With the [CERN Open Data client](https://cernopendata-client.readthedocs.io/en/latest/index.html) you can identify the files you wish to access, and either download a few of them locally or run on them remotely. Files from the ATLAS Open Data for research have file names like `root://eospublic.cern.ch//eos/opendata/atlas/rucio/`; in this file name, `root://eospublic.cern.ch/` can be replaced with `http://opendata.cern.ch`. You can try both `xrdcp` (with a filename beginning with `root://`) and `curl -O` (with a filename beginning with `https://`) for downloading. Which is faster will depend on the system.

Once you have a file to run on, feel free to try the notebooks described above. With `xrootd` installed, you can also remote-read a file (without downloading it in advance) by giving the full filename starting with `root://`. Remote reading also relies on the network between CERN and the computing resources you're using. For slower connections, downloading a file once and then operating on it locally will generally give a better experience.

At the [Nebraska-Lincoln](https://coffea-opendata.casa/) site, there is a special xcache instance which allows faster access to the open data on the CERN Open Data portal. In the file names you get from the Open Data client, replace `root://eospublic.cern.ch/` with `root://red-xcache1.unl.edu:1096/`, and you'll have a more responsive experience, especially in case you access a file that is already in the local cache.

On Google resources, we've found that xrootd is slow to build. Skipping its installation and using `curl` for downloading files seems to give a reasonable user experience.
