# Exploring Public Likelihoods
> 🚧 May 2024: Under construction

## Understanding the ATLAS Statistical Model

ATLAS produces a vast amount of data. Analysing this data requires sophisticated statistical techniques to draw meaningful conclusions about fundamental particles and their interactions. The statistical model, often represented as a likelihood function, is crucial in interpreting experimental results and extracting physical insights.

:::notebook [Analysing statistical workspaces with xRooFit](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/for-research/public-likelihoods/)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/will-cern/statanalysis-binder-demo/demo2?labpath=index.ipynb)
:::

## Key Components of the Model

- **Workspace inspection and visualisation**: Before diving into the statistical analysis, it's essential to inspect the workspace content and visualize relevant data to gain insights into the experimental setup.

- **Content extraction**: The extraction process involves isolating specific channels or subsets of data relevant to the analysis, focusing on areas of interest within the experimental data.

- **Reparameterisation**: Reparameterisation techniques are employed to transform the model parameters, simplifying the statistical analysis and improving interpretability.

- **Likelihood construction and minimisation**: The likelihood function encapsulates the probability of observing the experimental data given the model parameters. Minimising this likelihood provides estimates for the parameters and their uncertainties.

- **Impact and uncertainty breakdown**: Understanding the impact of individual parameters on the overall model and breaking down uncertainties are crucial steps in assessing the robustness of the analysis.

- **Profile likelihood scanning**: Profile likelihood scanning involves varying one or more parameters of interest while profiling out nuisance parameters, revealing insights into the parameter space and potential signal regions.

## Utilising the HS3 Format

To facilitate accessibility and reproducibility, ATLAS is adopting the HS3 format for publishing statistical models as JSON files. These files, along with an accompanying notebook, enable researchers to reproduce ATLAS results and perform their own analyses.

<!-- ### Run the notebook -->
<!-- [![Swan](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://cern.ch/swanserver/cgi-bin/go?projurl=https://github.com/pinamont/statistics-tutorial.git) -->