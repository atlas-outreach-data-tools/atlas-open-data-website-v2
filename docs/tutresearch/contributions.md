# Community Contributions
Here we gather various projects and analyses created using our open data for research. We believe in the power of collaboration and the insights that can emerge from diverse perspectives. If you've used our open data for something cool, we would love to hear about it! Please share your work with us through the [contact us](../../contact-us) form. Your contributions can inspire others and help to show the potential of open data.

A full list of uses of ATLAS Open Data can be found [on INSPIRE-HEP](https://inspirehep.net/literature?sort=mostrecent&size=25&page=1&q=references.reference.dois%3A10.7483%2FOPENDATA.ATLAS*).

## Notebooks
:::notebook [How the Scientific Python ecosystem helps answering fundamental questions of the Universe](https://github.com/ekourlit/scipy2024-ATLAS-demo)
By [Vangelis Kourlitis](https://github.com/ekourlit)  
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/ekourlit/scipy2024-ATLAS-demo/main?labpath=analysis_demo_coffea.ipynb)
:::
