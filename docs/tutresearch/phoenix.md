# Phoenix for Event Visualisation

Visualizing an event inside the detector opens a path to a deeper understanding of the physical processes resulting from a collision. One commonly used tool for event visualisation is [Phoenix](https://phoenixatlas.web.cern.ch/PhoenixATLAS/), a framework that allows 3D visualisation of collision events inside the detector.

<div class="responsive-iframe-container" style={{ borderRadius:'15px', minHeight: '50vh' }}>
    <iframe src="https://phoenixatlas.web.cern.ch/PhoenixATLAS/?file=data/OpenData/my_events.json&theme=dark&type=json" frameborder="0" allowfullscreen></iframe>
</div>

The event in the image above is an event for [gluino](https://en.wikipedia.org/wiki/Gluino) production, which in [SUSY](https://en.wikipedia.org/wiki/Supersymmetry) is the supersymmetric partner of a gluon. 

You can display any event of your choice by using the open data available in PHYSLITE format, and the process to do so is explained below:

## Transforming PHYSLITE or NTuple into JSON

Phoenix accepts data in JSON or JiveXML formats. We will focus on the [JSON format](https://github.com/HSF/phoenix/blob/main/guides/developers/event_data_format.md). To create the JSON from a PHYSLITE or NTuple file, we provide a python script:

<div class="centered-button-container">
  <a href="https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/for-research/phoenix" class="github-button">GitHub: PHYSLITE or NTuple to JSON</a>
</div>

To use the code, follow the steps below:

1. **Download a PHYSLITE or NTuple file**: The datasets can be found in the [CERN Open Data portal](https://opendata.cern.ch), and the instructions on how to search for one of our PHYSLITE datasets are in [Finding and Downloading Datasets](/docs/data/for_research/data_access). 
2. **Get code**: You can download the script and enviroment file, or clone the whole repository:
    ```
    git clone https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata.git
    ```
3. **Setup enviroment**: to setup the enviroment you can use the enviroment file with [Conda](https://docs.conda.io/en/latest/):
    ```
    conda env create -f path_to_file/environment_phoenix.yml
    ```
    Make sure to adjust `path_to_file` to the path where the code and the enviroment file are. If you don't use conda, these are the main libraries you will need:
    ```
    numpy==1.22.4
    uproot==4.3.7
    awkward==1.10.5
    coffea==0.7.21
    zstandard==0.22.0
    ```
4. **Run the script**: To see the options of the script, you can run 
    ```
    python path_to_file/get_json_phoenix.yml --help
    ```
    And you will see the options for running:
    ```
    usage: write_simple_json.py [-h] [-n NEVENTS] [-s NSKIP] [-o OUTPUTFILE]
                                [-l EVENTLIST]
                                filenames

    Writes events in simple json format for the Phoenix event display

    positional arguments:
    filenames             Comma-separated list of files for processing

    optional arguments:
    -h, --help            show this help message and exit
    -n NEVENTS, --nEvents NEVENTS
                            Number of events to run over (default 10)
    -s NSKIP, --nSkip NSKIP
                            Number of events to skip (default 0)
    -o OUTPUTFILE, --outputFile OUTPUTFILE
                            Name of output json file (default my_events.json)
    -l EVENTLIST, --eventList EVENTLIST
                            File containing run/event number pairs for outputting
    ```

    For example, for the gluino display, you can run the following code to generate the JSON file using 20 events:
    ```
    python get_json_phoenix.py path_to_file/mc20_13TeV.376518.MadGraphPythia8EvtGen_A14NNPDF23LO_GG_direct_2400_200.deriv.DAOD_PHYSLITE.e6732_s3797_r13167_p6026/DAOD_PHYSLITE.38191276._000001.pool.root.1 -n 20
    ```
    After running, you should have `my_events.json` in the folder where you ran the script, which now can be uploaded to Phoenix. 
    
You can open your own files in Phoenix using the file upload dialog (third, right to left, in the figure below)

import uploadPhoenix from './images/phoenix_bar.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={uploadPhoenix} alt="You can open your own files using the File upload dialog (third, right to left) " style={{width: 1000, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 1: Phoenix bar. The file upload dialog is the one inside the red square.</figcaption>
</figure>

After that, you should see your event!

For more information about the use of Phoenix, see the [Phoenix User Guide](https://github.com/HSF/phoenix/blob/main/guides/users.md).

## JSON examples
If you would like to explore more event types, we have provided JSON files for various physics processes. You can download these JSON files and open them in Phoenix to visualize the events.

- **Top Quark Events**
    - [Standard Model ttbar Production (non-all-hadronic)](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/for-research/phoenix/examples/ttbar_nonallhad.json)
    - [Triple Top Production](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/for-research/phoenix/examples/top_3top.json)
- **Weak Boson Events**
    - [ZZZ Production (4 leptons and 2 neutrinos)](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/for-research/phoenix/examples/ZZZ_4l2v.json)
- **Jet Events**
    - [High Jet Multiplicity Events](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/for-research/phoenix/examples/jet_yyy.json)
    - [Low Jet Multiplicity Events](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/for-research/phoenix/examples/jetjet_JZ10.json)
- **Higgs Events**
    - [tH Production with H→ZZ→4l](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/for-research/phoenix/examples/tH_ZZ4l.json)
    - [ttH Production with H→4l, ttbar Dileptonic Decay](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/for-research/phoenix/examples/ttH_ZZ4l.json)
- **Exotic Processes**
    - [Z'→μν](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/for-research/phoenix/examples/Zprime_mumu.json)
    - [Z'→ee](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/for-research/phoenix/examples/Zprime_ee.json)
    - [Z'→μμ](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/for-research/phoenix/examples/Zprime_mumu.json)

To view these events:
1. **Download the JSON file**: Click on the link corresponding to the event you are interested in and download from GitHub.
2. **Open Phoenix**: Launch the Phoenix event display in your web browser or use the embedded one above.
3. **Upload the JSON file**: Use the file upload dialog (third icon from the right on the toolbar) to upload the JSON file.
4. **Visualize the event**: The event will be displayed in Phoenix, allowing you to explore the detector interactions.