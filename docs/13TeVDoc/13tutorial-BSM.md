
# Physics Searches: Beyond Standard Model and Exotics
In these notebooks, we go beyond the  Standard Model to explore new theoretical frameworks and potential discoveries. The Beyond the Standard Model (BSM) physics encompasses theories like supersymmetry, extra dimensions, and dark matter candidates. These explorations are crucial for addressing the limitations of the Standard Model and opening new paths for discoveries in particle physics.

## Jupyter Notebooks
<!-- ### Python
#### [Seeking the Invisible: Dark Matter Search using cut-based optimisation](https://github.com/IDalziel/Dark-Matter-search-with-Cut-Based-Optimisation)
In this notebook, we will use simulated ATLAS detector data to search for evidence of dark matter.<br/>
Physics: ⭐⭐⭐<br/>
Coding: ⭐<br/>
Time: ⭐⭐<br/> -->

### Uproot
#### [Seeking the Invisible: Dark Matter Search with Neural Networks at ATLAS](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/uproot_python/Dark_Matter_Machine_Learning.ipynb)
This notebook deals with a search for Dark Matter with neural networks at the ATLAS Experiment at the LHC, using [ATLAS Open Data](http://opendata.atlas.cern).<br/>
Physics: ⭐⭐⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/HEAD?labpath=13-TeV-examples%2Fuproot_python%2FDark_Matter_Machine_Learning.ipynb)


#### [Search for the Graviton yourself!](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/blob/master/13-TeV-examples/uproot_python/GravitonAnalysis.ipynb)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you the steps to search for the Graviton yourself!<br/>
Physics: ⭐⭐<br/>
Coding: ⭐⭐⭐<br/>
Time: ⭐⭐⭐<br/>
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/atlas-outreach-data-tools/notebooks-collection-opendata/58190f0a5f15d35ade86708965403a0ed7b5a87e?urlpath=lab%2Ftree%2F13-TeV-examples%2Fuproot_python%2FGravitonAnalysis.ipynb)

