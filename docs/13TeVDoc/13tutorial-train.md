
# Training and statistics
Aside from the physic analysis, we share a set of useful resources to engage with the ATLAS open data.
## Jupyter Notebooks
### Python, ROOT/C++
#### [Statistics tutorial](https://github.com/pinamont/statistics-tutorial)
This notebook will allow you to go through the typical steps of a statistical analysis in high energy physics at particle colliders, inspecting the way to extract quantitative information from colision data. There are two main ways to run this tutorial: Binder and SWAN (the latter requires a CERN account).<br/>
Physics: ⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐⭐<br/>


## Kaggle Notenooks
### Python
#### [Model fitting](https://www.kaggle.com/code/meirinevans/model-fitting)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you some steps to prepare for model fitting.<br/>
Physics: ⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐<br/>

#### [Monte Carlo weights](https://www.kaggle.com/code/meirinevans/monte-carlo-weights)
This notebook uses [ATLAS Open Data](http://opendata.atlas.cern) to show you how to prepare MC weights on your way to Machine Learning.<br/>
Physics: ⭐⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐<br/>

### Uproot
#### [Uproot Tutorial](https://hsf-training.github.io/hsf-training-uproot-webpage/)
This tutorial aims to demonstrate how to quickly get started with Uproot, a Python package that can read and write files in the `.root` format without actually requiring or running the ROOT software at all (as opposed to PyROOT, which is just an interface in Python that runs ROOT behind the scenes).<br/>
Physics: ⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐⭐<br/>

#### [Matplotlib for HEP](https://hsf-training.github.io/hsf-training-matplotlib/)
This training module introduces matplotlib and creates plots commonly used in HEP. It also introduces [mplhep](https://github.com/scikit-hep/mplhep/), a plotting library designed specifically for HEP plots.<br/>
Physics: ⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐⭐<br/>

#### [Introduction to Machine Learning](https://hsf-training.github.io/hsf-training-ml-webpage/)
This tutorial explores Machine Learning using [ATLAS Open Data](http://opendata.atlas.cern) and scikit-learn and PyTorch for applications in high energy physics.<br/>
Physics: ⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐⭐<br/>

#### [Machine Learning on GPU](https://hsf-training.github.io/hsf-training-ml-gpu-webpage/)
This tutorial explores Machine Learning using GPU-enabled PyTorch for applications in high energy physics.<br/>
Physics: ⭐<br/>
Coding: ⭐⭐<br/>
Time: ⭐⭐⭐<br/>


## Local disk or virtual machines
### ROOT/C++
#### [Dataframe tutorials](https://root.cern/doc/master/group__tutorial__dataframe.html)
These examples are taken by the official ROOT reference guide and show various features of [RDataFrame](https://root.cern/doc/master/classROOT_1_1RDataFrame.html): [ROOT](https://root.cern/doc/master/namespaceROOT.html)'s declarative analysis interface. RDataFrame offers a high level interface for the analysis of data stored in [TTree](https://root.cern/doc/master/classTTree.html)s, [CSV files](https://root.cern/doc/master/classROOT_1_1RDF_1_1RCsvDS.html) and [other data formats](https://root.cern/doc/master/classROOT_1_1RDF_1_1RDataSource.html). In addition, multi-threading and other low-level optimisations allow users to exploit all the resources available on their machines transparently.<br/>
Physics: ⭐<br/>
Coding: ⭐⭐⭐<br/>
Time: ⭐<br/>

