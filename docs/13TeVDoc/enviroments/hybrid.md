# Hybrid Platforms
Hybrid platforms are ideal for users who intend to engage with ATLAS open data on a continuous basis. 
Moreover, some of these tools can be used even without an stable internet connection. 

By setting up a local environment, you can manage your data and analysis workflows more directly. 
However, this method is more time-consuming as it involves installing and configuring the necessary software and tools.

## Docker

Docker provides a robust platform for developing, sharing, and running applications within containers. ATLAS open data can be accessed through pre-configured Docker images for running the **analysis in notebooks**.

**Steps to run ATLAS open data using Docker:**

1. **Install Docker**: Download and install Docker from the [official website](https://docs.docker.com/get-docker/). 
2. **Open Docker**
3.  **Run the Docker Image**: Open your terminal and run the Docker image, available on our [GitHub registry](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/pkgs/container/notebooks-collection-opendata).
```
docker run -it -p 8888:8888 -v my_volume:/home/jovyan/work ghcr.io/atlas-outreach-data-tools/notebooks-collection-opendata:latest
```
4. **Launch the Notebook**: After running the Docker image, use the link provided in the terminal to access the Jupyter interface. The terminal should look like this:
```
To access the notebook interface, open this file in a browser:
        file:///home/jovyan/.local/share/jupyter/runtime/nbserver-15-open.html
    Or copy and paste one of these URLs:
        http://4c61742ed77c:8888/?token=34b7f124f6783e047e796fea8061c3fca708a062a902c2f9
     or http://127.0.0.1:8888/?token=34b7f124f6783e047e796fea8061c3fca708a062a902c2f9
```
5. **Select a Notebook and Upload**: Navigate to either the [Physics Searches: Standard Model](../13tutorial) or [Physics Searches: Beyond Standard Model and Exotics](../13tutorial-BSM) sections, choose your desired analysis and download. To download the notebook, click on "donwload raw file", in the top right corner of the file. 

import githubdownload from '../pictures/download_github.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={githubdownload} alt="donwload raw file button in github" style={{width: 300, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 1: "Donwload raw file" button in GitHub.</figcaption>
</figure>

Under the "files" tab, click on "Upload" and select the file you want to upload. You will see the "Upload" button on the right side of the screen:

import jupyterupload from '../pictures/jupyter_upload.png';

<figure style={{textAlign: 'center', marginBottom: '20px'}}>
    <img src={jupyterupload} alt="Upload button in the Jupyter interface" style={{width: 300, marginBottom: '0px'}} />
    <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>Fig. 2: "Upload" button in the Jupyter interface.</figcaption>
</figure>

6. **Clone the Whole Repository**: Alternatively, you can get the entire repository inside the container. For example, you can get all the notebooks by running the following inside the container terminal:
```
git clone https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata.git
```
7.  **Begin Analysis**: Start your data analysis within the Docker environment.

## Virtual Machines

The 13 TeV ATLAS Open Data [virtual machine](https://en.wikipedia.org/wiki/Virtualization) (VM) allows you to create a virtual (rather than actual) operating system and test the open data on your own host machine. This enviroment is aimed to **running the notebooks and the frameworks**.

### Virtual machine installation
To use the virtual machines we need to install: 
1. Oracle VirtualBox.
2. The virtual machine itself.
#### 1. Oracle VirtualBox installation
The Oracle VirtualBox is the chosen software that supports Microsoft Windows, Linux, FreeBSD, Mac OS X, Solaris / OpenSolaris, ReactOS, DOS and other systems. Donwload the software from the [oficial website](https://www.virtualbox.org). Follow the instructions below to **install VirtualBox** on your computer:

---

**Check our ~3 min video tutorial on how to install VirtualBox**

<div class="iframe-container">
    <iframe scrolling="no" src="https://videos.cern.ch/video/OPEN-VIDEO-2020-018-001" width="80%" height="431" frameborder="0" allowfullscreen></iframe>
</div>

---

<details>
<summary> Click here to see the instructions in images.</summary>

1. **Donwload VirtualBox**: Download VirtualBox for free on the [official website](https://www.virtualbox.org/).

2. **Install VirtualBox**. Find the downloaded file in your computer (usually the downloaded file is in *С:\Users\User\Downloads\"filename"*), run the program and click "Next".

![path](../pictures_vm/fig-1.png)

After that, the component selection window will appear. Without changing anything, click "Next".

![path](../pictures_vm/fig-2.png)

In the next window, without any changing click "Next"

![path](../pictures_vm/fig-3.png)

Now a window will appear that says that the Internet will be temporarily disabled during the installation of the program. Click "Yes".

![path](../pictures_vm/fig-4.png)

And click "Install" to begin the installation.

![path](../pictures_vm/fig-5.png)

After the installation process is complete, click "Finish"

![path](../pictures_vm/fig-6.png)

Now you will see a clean window of your virtual machine without any operating systems:

![path](../pictures_vm/fig-6-1.png)

3. **Configure VirtualBox**: If you have English installed on your system, the program will automatically change the interface language to English when you first start it. If this does not happen, go to the menu *“File” -> “Settings or Preferences”* and on the *Languages* tab select your *language*.

![path](../pictures_vm/fig-7.png)

</details>

#### 2. Virtual machine installation

1. **Download the latest ATLAS Open Data virtual machine**

import downloadvm from '../pictures_vm/download_vm.png';

<a href="https://zenodo.org/record/3687320/files/ATLAS-Open-Data-ubuntu-2020-v4.ova">
    <figure style={{textAlign: 'center', marginBottom: '20px'}}>
        <img src={downloadvm} alt="button to download virtual machines" style={{width: 300, marginBottom: '0px'}} />
        <figcaption style={{fontStyle: 'italic', fontSize: '14px', marginTop: '0px'}}>
            Click here to download the virtual machine.
        </figcaption>
    </figure>
</a>

:::note System Configuration
This is an **Ubuntu 18.04.3 LTS** with:

- **ROOT** 6.18 (configuration all)
- **Jupyter** (bash, python2, python3, ROOT C++ kernels)
- **Extras** TensorFlow + demo git repos
- **Cite with** [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3629875.svg)](https://doi.org/10.5281/zenodo.3629875)
- The password of the VM is **root** (also to be `sudo`)
:::
---
**Check our ~9 min video tutorial on how to install the virtual machine**

<div class="iframe-container">
    <iframe scrolling="no" src="https://videos.cern.ch/video/OPEN-VIDEO-2020-018-002" width="80%" height="431" frameborder="0" allowfullscreen></iframe>
</div>
---

<details>
<summary> Click here to see the instructions in images </summary>

+ After you downloaded the VM file, start the VirtualBox. Go to the menu ***"File"*** **->** ***“Import Appliance...”*** (or perform the same function with the combination of buttons: **"Ctrl" + "I"**).

![path](../pictures_vm/fig-7-1.png)

+ In the window, select the downloaded ".ova" file from the **Downloads** folder and click "Next":

![path](../pictures_vm/fig-7-2.png)

+ Click "Import" without any changes:

![path](../pictures_vm/fig-7-3.png)

+ The operating system should begin importing:

![path](../pictures_vm/fig-7-4.png)

+  Now your Virtual Machine with Ubuntu-Linux operating system is ready, start it by clicking ![path](../pictures_vm/fig-7-5.png) and use:

![path](../pictures_vm/fig-7-6.png)

</details>

:::caution Hardware Virtualization Issues
You may come across issues with hardware virtualisation. Some computers have virtualisation disabled by default. If you do, and due to the various manufacturers, the best is to Google something like: "how to enable virtualisation in *[insert name and model of your computer]*".

For example, here is a solution for Lenovo:
- Boot systems to BIOS with the F1 key at power on.
- Select the Security tab in the BIOS.
- Enable Intel VTT or Intel VT-d if needed.
- Once enabled, save the changes with F10 and allow the system to reboot.
:::

---
### How to Run the Virtual Machine

**Take a look to this video on how to get and run some notebooks and framework in your computer**

<div class="iframe-container">
    <iframe scrolling="no" src="https://www.youtube.com/embed/Lj73Vjd6Nys?start=52" width="80%" height="431" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

---

<!--
_ _ _
**Information:** If you want to install your operating system (in this case Ubuntu) from scratch, then see how to do it below.
_ _ _


After starting your VM and running the notebooks, you may encounter the following **error** (look at the red frames in the photo below):

![path](../13TeVDoc/pictures_vm/fig-12-3.png)

To solve this **error**, follow these simple steps:

+ Сlose notebooks and terminal

+ Run the terminal again and type the following code: * `./run-server-jupyter.sh` * to start Jupiter

![path](../13TeVDoc/pictures_vm/fig-12-4.png)

+ This way you have restarted your terminal and server successfully

![path](../13TeVDoc/pictures_vm/fig-12-5.png)

+ Open the notebook in your browser and restart the *kernel* by clicking the restart button

![path](../13TeVDoc/pictures_vm/fig-12-6.png)

+ Now your terminal and server should be working and the notebooks should run without errors

![path](../13TeVDoc/pictures_vm/fig-12-7.png)


<details>
<summary> **Create a virtual machine containing Ubuntu** </summary>

— Click the "New" button.

![path](../pictures_vm/fig-8.png)

— In the settings window specify the "Name" (in the future you can change), the "Type" and "Version" of the operating system and click "Next".  For example - Name: My_Ubuntu (o_0), Type: Linux, Version: Ubuntu (64-bit).

![path](../pictures_vm/fig-9.png)

— The machine claims that the minimum memory size (RAM) requirements for full-fledged work Linux,, 1024MB. But if you want your machine to work well, then select the volume for more depending on the RAM of your computer (2 GB if RAM of your computer 8 GB) and click "Next". Sometimes even more, it all depends on the tasks and how many Virtual Machines you will run at the same time.

![path](../pictures_vm/fig-10.png)

— So we went to create a virtual hard disk. Click "Create":

![path](../pictures_vm/fig-10-1.png)

— Specify the type of hard disk (VDI - VirtualBox Disk Image) and click "Next":

![path](../pictures_vm/fig-11.png)
(Choose the first item, as it is easier. You have created a file that will simulate the hard disk of your virtual machine. If necessary, you can transfer it to another computer and run there).

— In the window that appears, put a tick in "Dynamically allocated", after click "Next":

![path](../pictures_vm/fig-12.png)

-The  hard disk size set at least 80 GB (or more) and click "Create".

![path](../pictures_vm/fig-12-1.png)

— Now the Virtual Machine is ready, but without an operating system. Start the Virtual Machine by clicking on the right mouse button and select **"Start" -> "Normal start"**.

![path](../pictures_vm/fig-12-2.png)

— When you first start you will be prompted to select the installation file from which the installation will be performed. You can download the installation file from the [official website](https://ubuntu.com/download/desktop). Select the installation file from the Downloads folder (or where you saved it) and click "Start".

![path](../pictures_vm/fig-13.png)

— In the window that appears, select the operating system language and click on "Install Ubuntu". Next, select the keyboard language.

![path](../pictures_vm/fig-14.png)

— After selecting the keyboard language, just click on "Continue"

![path](../pictures_vm/fig-15.png)

— *Disk layout*. You can choose installation type. Simply format the entire hard drive and install Ubuntu on it, or select the manual option (Set the checkbox to "Something else" option and click "Continue"):

![path](../pictures_vm/fig-16.png)

— In the opened window, if you have not yet marked up the hard drive, you need to create a partition table, to do this, click "New Partition Table...".

![path](../pictures_vm/fig-17.png)

— Four sections are recommended for Linux:

* / - ext4, size 10-50 GB, for system installation
* /boot - ext2, size 100 MB, for bootloader files
* swap - swap, size equals RAM, for swap
* /home - ext4, all remaining space

— To create a new section, click the "+" button:

![path](../pictures_vm/fig-18.png)

— Here you need to specify the mount point, for example, **/** or **/home**, size, file system and you can set a label:

 ![path](../pictures_vm/fig-19.png)

— In the end, you should have something like this:

![path](../pictures_vm/fig-20.png)
Then click "Install Now".

— You should have this window:

![path](../pictures_vm/fig-21.png)
Check that everything is correct, click "Continue".

— Select your time zone:

![path](../pictures_vm/fig-22.png)

— Enter your Username, computer name and password:

![path](../pictures_vm/fig-23.png)

— Installing your operating system:

![path](../pictures_vm/fig-24.png)

— Enjoy your virtual machine:

![path](../pictures_vm/fig-25.png)

</details>

-->