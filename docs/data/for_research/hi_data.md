# Heavy Ion Collisions Data
Heavy ion analysis is part of the program of the ATLAS Experiment. It consist on colliding lead (Pb) nucleus to try recreating and understand the conditions that existed in the universe just after the Big Bang.

## Minimum Bias Stream
We shared the whole data obtained in the 2015 run, that adds to over 200 million collisions. These data is stored in [HION14 format](/docs/documentation/data_format/HI_formats#minimum-bias-data-format), a light format with all the information needed for simple analyses. 

In the [main entrypoint](https://opendata.cern.ch/record/80035) for the heavy ion data you will find two links:
- [Detector data](https://opendata.cern.ch/record/80036): The data obtained from the real collisions, measured by the ATLAS detector
- [Simulations](https://opendata.cern.ch/record/80037): Simulated collisions of heavy ion.