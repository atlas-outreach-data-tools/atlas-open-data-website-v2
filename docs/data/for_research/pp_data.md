# Proton-Proton Collisions Data
The ATLAS Collaboration released over 6 billion proton-proton collisions with accompanying simulations. All these events were released in [PHYSLITE format](/docs/documentation/data_format/physlite), a light format used internally for analysis. 

## Search for Data
Inside the [main entry point](https://opendata.cern.ch/record/80020) of proton-proton collision open data you will find eleven different links that point to different datasets. These are explained below.

We have two datasets with detector data:
- [Run 2 2015 proton-proton collision data](https://opendata.cern.ch/record/80000): Detector data for the 2015 run, with an integrated luminosity of ~3.2 fb$^{-1}$
- [Run 2 2016 proton-proton collision data](https://opendata.cern.ch/record/80001): Detector data for the 2016 run, with an integrated luminosity of ~33 fb$^{-1}$

We have seven groups for Standard Model Monte Carlo samples. Four for nominal samples:
- [MC simulation electroweak boson nominal samples](https://opendata.cern.ch/record/80010).
- [MC simulation Higgs nominal samples](https://opendata.cern.ch/record/80012).
- [MC simulation QCD jet nominal samples](https://opendata.cern.ch/record/80014).
- [MC simulation top nominal samples](https://opendata.cern.ch/record/80017).

Three for alternative samples for the calculation of [systematic variations](/docs/documentation/systematics/systematics.md) (since the systematics for electroweak boson are included in the nominal datasets):
- [MC simulation Higgs systematic variation samples](https://opendata.cern.ch/record/80013).
- [MC simulation QCD jet systematic variation samples.](https://opendata.cern.ch/record/80015) 
- [MC simulation top systematic variation samples](https://opendata.cern.ch/record/80018).

And two groups for Beyond the Standard Model signal samples:
- [MC simulation SUSY signal samples](https://opendata.cern.ch/record/80016).
- [MC simulation exotic signal samples](https://opendata.cern.ch/record/80011).