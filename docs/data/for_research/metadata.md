import DownloadLink from '@site/src/components/DownloadLink';
import React from 'react';
import FilterableTable from '@site/src/components/FilterableTable';

# Monte Carlo Metadata
Below, you will find the metadata for all the samples, which includes comprehensive information such as:

- **Dataset ID**: This is a unique identifier assigned to each dataset. It ensures that each dataset can be uniquely referenced and accessed.
- **Physics Short**: This is an abbreviated name that provides key information about the dataset. To know more about how to read them, check the subsection about [naming convention](/docs/data/for_research/data_access#naming-convention).
- **Cross-Section (in pb)**: Represents the probability of a particular interaction occurring, measured in picobarns (pb). It is a fundamental parameter that helps understanding the likelihood of specific particle interactions under given conditions.
- **Filter Efficiency**: Measure of the effectiveness of the selection criteria applied to the data. It indicates the fraction of events that pass the filters applied during the data processing stages.
- **K-factor**: Multiplicative correction factor used to account for higher-order effects in theoretical calculations. It adjusts the leading-order theoretical predictions to better match the observed data by incorporating next-to-leading order (NLO) or next-to-next-to-leading order (NNLO) corrections.
- **Number of Events**: This refers to the total count of simulated events in the dataset. Each event represents a single instance of a particle interaction or decay as captured by the detector or simulated by software.
- **Sum of Weights**: In datasets where events are assigned weights, this value represents the cumulative total of these weights across all events in the dataset. This is useful for normalizing the dataset to represent physical quantities like cross-sections more accurately.
- **Sum of Weights Squared**: This value represents the sum of the squares of the event weights. It is particularly useful in statistical analyses, such as calculating the variance and uncertainty in measurements derived from weighted events.
- **Process Description**: Provides a brief description of the physics process being studied. For instance, "H->$\gamma\gamma$" denotes the Higgs boson decaying into two photons.
- **Generators Used**: Specifies the simulation software used to generate the data. Information about the generators can be found in the [Simulation Tools section](/docs/documentation/monte_carlo/simulation_tools)
- **Keywords**: These are specific terms or phrases associated with the dataset that help to find specific datasets. Some examples of keywords are: "SM", "BSM", "Higgs".
- **Description**: Provides additional information about the dataset. It may include details about the experimental setup, conditions under which the data was simulated, or any special characteristics of the dataset.
- **Job Option**: This includes a link to the specific code or configuration files used to generate the sample.

Use the search bar to find a specific dataset by its ID, keywords, or description. The search bar can locate any text within the table, allowing for flexible and comprehensive searches. You can also **download the csv version of the table <DownloadLink file="/files/metadata.csv">here</DownloadLink>**.

<FilterableTable />
