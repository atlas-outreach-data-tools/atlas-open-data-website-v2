# 13 TeV Data for Research

This is ATLAS collaboration's first release of open data for research. In compliance with the established [Open Data Policy](https://opendata.cern.ch/docs/cern-open-data-policy-for-lhc-experiments), ATLAS is releasing 25% of the 2015 and 2016 proton–proton main physics stream datasets. However, this effort is part of a broader commitment to open science, facilitating new paths of exploration across the scientific community.

<div class="centered-button-container">
  <a href="https://opendata.cern.ch/record/80020" class="download-vm-button" target="_blank">Explore the 13 TeV Data for Research</a>
</div>

## Overview of the Data
The selection of data for release includes the 2015 and 2016 runs, offering real collision data and Monte Carlo (MC) simulations that support a wide array of general analyses. The datasets span Standard Model processes, systematic uncertainty evaluations, and Beyond the Standard Model (BSM) signal processes, based on benchmark models from recent publications. 

The measurements from proton-proton collisions and lead nucleus collisions were selected based on the [Good Run Lists](/docs/documentation/data_collection/GRL_definition) (GRLs), ensuring high quality and reliability for scientific exploration.

The MC samples, part of the MC20a simulation campaign, are tailored to represent double the luminosity of the corresponding data to ensure comprehensive coverage for analysis. It includes Standard Model nominal samples and alternative for systematic variations, categorized as follows:
- **Top**: Processes containing top quarks.
- **Jets**: Processes that involve the creation of jets. 
- **Higgs boson**: Processes that include the Higgs boson.
- **Weak boson**: Processes including the Z or W bosons.

And Beyond the Standard Model Signals in the following groups:

- **SUSY**: Signals from super-symmetry models.
- **Exotics**: Other non-Standard Model signals.

For Heavy Ion research, the 2015 lead-lead data, constituting approximately 25.3% of the total Run 2 lead-lead data, will be released in a similar format. This format will include specific heavy ion event information, such as track four-vectors and centrality.

To understand how to search for data, check the [next section](/docs/data/for_research/data_access).
