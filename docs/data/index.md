import DocCardList from '@theme/DocCardList';

# The Data
The ATLAS collaboration, dedicated to advancing scientific knowledge and promoting open science, has made a significant portion of its data publicly available. This release includes two primary types of data:

- [Monte Carlo (MC) Simulations](/docs/documentation/monte_carlo/introduction_MC.md): Simulated particle collisions within the ATLAS detector.
- [Detector Data](/docs/documentation/data_collection/data_collection.md): Measurements recorded from particle collisions within the ATLAS detector.

These datasets provide valuable insights into high-energy particle physics and are available in two levels of complexity: for educational purposes and for research. They are good resources for researchers, educators, and students, supporting a wide range of scientific inquiries, educational projects, and exploratory analyses.

For detailed information on the data content and how to download it, explore the categories below:

<br/>
<DocCardList />
