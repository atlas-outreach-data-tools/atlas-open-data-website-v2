# How to Use the Data
In the previous section, you can navigate the interactive dataset explorer to browse and select datasets for download. However, we recommend using the data remotely.

## ATLAS Digital Archive
### Retrieving File URLs
To obtain the URL of a specific dataset, simply right-click on your chosen dataset in the navigator and select "Copy link." If you wish to download the data, choose the `.zip` version of the dataset. For streaming the data directly, you can systematically construct the URL.

The base URL is `https://atlas-opendata.web.cern.ch/Legacy13TeV`, which hosts all the datasets. For example, if you want to access the 2-lepton samples, look for the "2lep" directory in the navigator, where you'll find subfolders for Data and MC. Suppose you want to access an MC file named `mc_301323.ZPrime500_tt.2lep.root`. To stream this file, the full URL would be:
```
https://atlas-opendata.web.cern.ch/Legacy13TeV/MC/mc_301323.ZPrime500_tt.2lep.root
```

### Accessing Data
You can stream the data directly into your code without downloading the full dataset to your local machine. Here’s a Python example using `ROOT` to open a file:
```python
import ROOT

file_url = "https://atlas-opendata.web.cern.ch/Legacy13TeV/MC/mc_301323.ZPrime500_tt.2lep.root"
file = ROOT.TFile.Open(file_url)
```
This approach allows for efficient access to large datasets hosted remotely on ATLAS's servers.

## Use examples
For examples on how to retrieve the data and use it for analysis check our [13 TeV tutorials](/docs/category/13-tev-tutorials-for-education).
