# How to Use the Data
The 8 TeV data for education is available on the [CERN Open Data Portal](https://opendata.cern.ch/search?q=atlas%20release%202016&f=type%3ADataset&l=list&order=asc&p=1&s=10&sort=bestmatch). You can download the files directly from the portal, but **we recommend remote data access**.

## CERN Open Data Client (recommended)
For remote data access, you can use the [CERN Open Data Client](https://cernopendata-client.readthedocs.io/en/latest/), leveraging the `xrootd` protocol. This will allow you to retrieve the necessary URLs and access the data directly from CERN’s storage systems.

Install the `cernopendata-client` package along with the `fsspec-xrootd` package by running:

```bash
pip install cernopendata-client fsspec-xrootd
```

### Retrieving File URLs
Once the client is installed, you can use it to obtain the URLs of the data files, for example, via their [DOI (Digital Object Identifier)](https://en.wikipedia.org/wiki/Digital_object_identifier). To get the file URLs from [this record](https://opendata.cern.ch/record/3820), you can use the following command:

```bash
cernopendata-client get-file-locations --doi 10.7483/OPENDATA.ATLAS.TC5G.AC24 --protocol xrootd
```
This command will return a file location like the following:

```bash
root://eospublic.cern.ch//eos/opendata/atlas/OutreachDatasets/2016-07-29/MC/mc_147770.Zee.root
```

### Accessing Data
Using the `xrootd` protocol, you can stream the data directly into your code without downloading the full dataset to your local machine. Here’s a Python example using `ROOT` to open a file:
```python
import ROOT

file_url = "root://eospublic.cern.ch//eos/opendata/atlas/OutreachDatasets/2016-07-29/MC/mc_147770.Zee.root"
file = ROOT.TFile.Open(file_url)
```
This approach allows for efficient access to large datasets hosted remotely on CERN's servers.

## ATLAS Digital Archive (deprecated)
### Retrieving File URLs
The 8 TeV data can also be found in the [ATLAS Open Digital Archive](https://atlas-opendata.web.cern.ch/Legacy8TeV/). Once inside the databes, if you wish to download the data, choose the `.zip` version of the dataset to obtain the URL of a specific dataset. For getting a specific file, simply right-click on your chosen dataset and select "Copy link.". For streaming multiple files, you can systematically construct the URL.

The base URL is `https://atlas-opendata.web.cern.ch/Legacy8TeV`, which hosts all the datasets. For example, if you want to access the Monte Carlo sample for the decay of the Z boson into two electrons, you go to the *MC* folder. The file corresponding to Z$rightarrow$ee is `mc_147770.Zee.root`. To stream this file, the full URL would be:
```
https://atlas-opendata.web.cern.ch/Legacy8TeV/MC/mc_147770.Zee.root
```

### Accessing Data
You can stream the data directly into your code without downloading the full dataset to your local machine. Here’s a Python example using `ROOT` to open a file:
```python
import ROOT

file_url = "https://atlas-opendata.web.cern.ch/Legacy13TeV/MC/mc_301323.ZPrime500_tt.2lep.root"
file = ROOT.TFile.Open(file_url)
```
This approach allows for efficient access to large datasets hosted remotely on ATLAS's servers.

## Use examples
For examples on how to retrieve the data and use it for analysis check our [8 TeV tutorials](/docs/category/8-tev-tutorials-for-education).
