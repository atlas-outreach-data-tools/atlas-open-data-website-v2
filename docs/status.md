# Status of the Jupyter notebooks
*This page is created automatically, for more information please visit [our repository](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/actions/runs/11232037948).*
### cpp
| Status | Link |
|----------|----------|
✅|[ATLAS_OpenData_13-TeV_simple_cpp_example_histogram.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/cpp/ATLAS_OpenData_13-TeV_simple_cpp_example_histogram.ipynb)

### uproot
| Status | Link |
|----------|----------|
❌|[CoffeaHZZAnalysis.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/uproot_python/CoffeaHZZAnalysis.ipynb)
❌|[Dark_Matter_Machine_Learning.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/uproot_python/Dark_Matter_Machine_Learning.ipynb)
✅|[Find_the_Z.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/uproot_python/Find_the_Z.ipynb)
✅|[GravitonAnalysis.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/uproot_python/GravitonAnalysis.ipynb)
✅|[HZZAnalysis.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/uproot_python/HZZAnalysis.ipynb)
✅|[HZZ_BDT_demo.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/uproot_python/HZZ_BDT_demo.ipynb)
✅|[HZZ_NeuralNet_demo.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/uproot_python/HZZ_NeuralNet_demo.ipynb)
✅|[HyyAnalysisNew.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/uproot_python/HyyAnalysisNew.ipynb)
✅|[learning_rate_demo.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/uproot_python/learning_rate_demo.ipynb)
✅|[ttZ_ML_from_csv.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/uproot_python/ttZ_ML_from_csv.ipynb)
❌|[ttZ_ML_from_root.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/uproot_python/ttZ_ML_from_root.ipynb)

### python
| Status | Link |
|----------|----------|
✅|[ATLAS_OpenData_13-TeV_python_full_HyyAnalysis_5min.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/python/ATLAS_OpenData_13-TeV_python_full_HyyAnalysis_5min.ipynb)
✅|[ATLAS_OpenData_13-TeV_python_invariant_mass_reconstruction_using_TLorentz_vectors.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/python/ATLAS_OpenData_13-TeV_python_invariant_mass_reconstruction_using_TLorentz_vectors.ipynb)
✅|[ATLAS_OpenData_13-TeV_simple_python_example_histogram.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/python/ATLAS_OpenData_13-TeV_simple_python_example_histogram.ipynb)
✅|[ATLAS_OpenData__13-TeV_analysis_example-python_Hyy_channel.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/python/ATLAS_OpenData__13-TeV_analysis_example-python_Hyy_channel.ipynb)

### RDataFrame
| Status | Link |
|----------|----------|
✅|[RDataFrame_Hyy.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/13-TeV-examples/rdataframe/RDataFrame_Hyy.ipynb)

### public-likelihoods
| Status | Link |
|----------|----------|
✅|[index.ipynb](https://github.com/atlas-outreach-data-tools/notebooks-collection-opendata/tree/master/for-research/public-likelihoods/index.ipynb)

*Last update on Tue Oct  8 11:19:33 CEST 2024*
