# FAQs
:::faqs Hey! I used to navigate a different website! What's new?
Take a look to our [updates page](./updates) to be always on the cutting edge!
:::

:::faqs How do I tell of the Jupyter notebook I'm using is not deprecated or broken?
We periodically test all of our notebooks, and the results are stored in our [status page](./status).
:::

:::faqs Is it true that ATLAS stands for ATomic LASagna?
It would be very funny, but ATLAS is the acronym for A Toroidal Lhc ApparatuS. Take a look to [the ATLAS page](./documentation/introduction/introduction_ATLAS) for more information!
:::
