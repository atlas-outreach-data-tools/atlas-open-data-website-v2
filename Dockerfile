FROM openshift/nodejs:20-ubi9
USER root
# Set the working directory inside the container
WORKDIR /app

# Copy the package.json and yarn.lock files to install dependencies
COPY package.json ./

# Install dependencies
RUN npm install -g yarn && yarn install

# Copy the rest of the application code
COPY . .

# Build the Docusaurus website
RUN yarn build

# Set the permissions for the application to be run by an arbitrary user
RUN chgrp -R 0 /opt/app-root/src && \
    chmod -R g=u /opt/app-root/src

## Expose the port that Docusaurus will run on.
EXPOSE 8080
## Run the production server.
CMD ["yarn", "serve", "--host", "0.0.0.0", "--port", "8080", "--no-open"]
