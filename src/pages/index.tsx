import React from "react";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import Layout from "@theme/Layout";
import atlasLogo from "./collision.gif";
import bottom from "./bottom.png";
import accessibility from "./accessibility.webp";
import expertise from "./expertise.webp";
import usability from "./usability.webp";
import "./index.css";

function HomepageHeader() {
  const { siteConfig } = useDocusaurusContext();
  return (
    <div className="container-section">
      <div style={{ height: "auto" }}>
  <center>
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <h1
        style={{
          fontWeight: "400",
          fontSize: "4em",
          marginBottom: "20px",
          marginTop: "20px",
        }}
        className="title is-1 is-bold is-spaced"
      >
        ATLAS Open Data
      </h1>
    </div>
    <div className="image-landing">
      <img 
        src={atlasLogo} 
        alt="ATLAS Logo" 
        style={{ maxWidth: "600px", height: "auto", width: "100%" }} 
      />
    </div>
    <div style={{ width: "auto" }}>
      <h2
        style={{
          color: "var(--ifm-atlas-color)",
          fontWeight: "400",
          fontSize: "2.5em",
          marginBottom: "20px",
        }}
        className="subtitle is-5 is-muted"
      >
        High Energy Physics data for everyone.
      </h2>
    </div>
  </center>
</div>


      <div className="container">
        <center>
          <div style={{ display: 'flex', justifyContent: 'space-between' }}>
            <div style={{ background: 'var(--ifm-color-primary-lighter)', padding: '20px', marginRight: '10px', borderRadius: '15px', flex: 1 }}>
              <h1 style={{ color: 'var(--ifm-color-primary-darkest)', fontWeight: 400, marginTop: '20px' }} className="subtitle is-5 is-muted">For Education</h1>
              <p style={{ color: 'var(--ifm-color-primary-darkest)' }}>
                To provide data and tools to high school, undergraduate and graduate students, as well as teachers and
                lecturers, to help educate them and exercise in physics analysis
                techniques used in experimental particle physics.
              </p>
            </div>
            <div style={{ background: 'var(--ifm-color-primary-lighter)', padding: '20px', borderRadius: '15px', flex: 1 }}>
              <h1 style={{ color: 'var(--ifm-color-primary-darkest)', fontWeight: 400, marginTop: '20px' }} className="subtitle is-5 is-muted">For Research</h1>
              <p style={{ color: 'var(--ifm-color-primary-darkest)' }}>
                To provide researchers with high-quality data recorded by the ATLAS detector, enabling them to conduct state of the art analyses in particle physics.
              </p>
            </div>
          </div>
          <div className="get-started">
            <a href="docs/category/get-started" style={{ textDecoration: 'none' }}>
              <b>GET STARTED</b>
            </a>
          </div>

          <h1 style={{ color: 'var(--ifm-atlas-color)', fontWeight: 550, fontSize: '2.9em', marginBottom: '20px',marginTop: '90px' }} className="subtitle is-5 is-muted">
            Our values
            </h1>
          <p style={{ fontSize: "1.2rem" }}>The collaboration shares the data gathered by the ATLAS detector
            committing to three fundamental principles:</p>
          </center>
        <div className="container-grid">
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              marginTop: "-5em",
              alignItems: "center",
              height: "100%",
            }}
            className="container-column left"
          >
            <div className="floating-box">
              <h2
                style={{
                  color: "var(--ifm-atlas-color)",
                  fontWeight: "400",
                  fontSize: "2.2em",
                }}
                className="subtitle is-5 is-muted"
              >
                Accessibility
              </h2>
              <p style={{ fontSize: "1.2rem" }}>
                Make the data and the tools openly available for everyone to
                use, without technology, region, or knowledge restrictions.
              </p>
            </div>
          </div>

          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              marginBottom: "1.5em",
              height: "100%",
            }}
            className="container-column right"
          >
            <img
              style={{ width: "60%" }}
              src={accessibility}
              alt="ATLAS Logo"
            />
          </div>
        </div>
      </div>

      <div className="container">
        <div className="container-grid">
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              marginBottom: "1.5em",
              height: "100%",
            }}
            className="container-column"
          >
            <img style={{ width: "70%" }} src={expertise} alt="ATLAS Logo" />
          </div>

          <div
            style={{
              display: "flex",
              justifyContent: "center",
              marginTop: "-5em",
              alignItems: "center",
              height: "100%",
            }}
            className="container-column"
          >
            <div className="floating-box">
              <h2
                style={{
                  color: "var(--ifm-atlas-color)",
                  fontWeight: "400",
                  fontSize: "2.2em",
                }}
                className="subtitle is-5 is-muted"
              >
                Transferable expertise
              </h2>
              <p style={{ fontSize: "1.2rem" }}>
                Along with particle physics analysis and ATLAS learning
                objectives, provide skills in programming, software and machine
                learning.
              </p>
            </div>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="container-grid">
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              marginTop: "-5em",
              alignItems: "center",
              height: "100%",
            }}
            className="container-column left"
          >
            <div className="floating-box">
              <h2
                style={{
                  color: "var(--ifm-atlas-color)",
                  fontWeight: "400",
                  fontSize: "2.2em",
                }}
                className="subtitle is-5 is-muted"
              >
                Usability
              </h2>
              <p style={{ fontSize: "1.2rem" }}>
                Different target audiences, with different backgrounds and
                skills must be able to use the data and tools for a wide range
                of learning objectives.
              </p>
            </div>
          </div>

          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              marginBottom: "1.5em",
              height: "100%",
            }}
            className="container-column right"
          >
            <img
              style={{ borderRadius: "10px", width: "60%" }}
              src={usability}
              alt="ATLAS Logo"
            />
          </div>
        </div>
        <center>
          <div
            style={{
              marginTop: "5em",
              marginBottom: "5em",
            }}
          >
          </div>
        </center>
      </div>
    </div>
  );
}

export default function Home(): JSX.Element {
  const { siteConfig } = useDocusaurusContext();
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Documentation the ATLAS Open Data project."
    >
      <HomepageHeader />
      <main></main>
    </Layout>
  );
}
