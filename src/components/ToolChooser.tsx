import React from 'react';

function ToolChooser() {
  return (
    <iframe 
      src="http://opendata.atlas.cern/ROOT_execute/ROOTBooks_execute_mainframe.html" 
      height="720px" 
      width="100%" 
      style={{padding: "0% 0% 0% 0%"}} 
      frameborder="0">
    </iframe>
  );
}

export default ToolChooser;
