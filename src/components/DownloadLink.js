import React from 'react';

const DownloadLink = ({ file, children }) => (
  <a href={file} download={file.split('/').pop()}>{children}</a>
);

export default DownloadLink;
